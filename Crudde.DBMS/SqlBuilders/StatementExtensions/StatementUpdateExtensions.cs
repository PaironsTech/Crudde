﻿using System.Collections.Generic;
using System.Data;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Static class with extension methods of "StatementUpdate"
    /// </summary>
    public static class StatementUpdateExtensions
    {

        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static bool BindAndExecuteGetBool<TModelToUpdate>(this SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable
        {
            return connection.GetAdapter().BindAndExecuteGetBool(sqlBinderUpdate, connection, transaction, commandTimeout, commandType);
        }


        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static int BindAndExecuteGetInt<TModelToUpdate>(this SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable
        {
            return connection.GetAdapter().BindAndExecuteGetInt(sqlBinderUpdate, connection, transaction, commandTimeout, commandType);
        }


        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static IEnumerable<TModelToUpdate> BindAndExecuteGetTModel<TModelToUpdate>(this SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable
        {
            return connection.GetAdapter().BindAndExecuteGetTModel(sqlBinderUpdate, connection, transaction, commandTimeout, commandType);
        }

    }

}
