﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Static class with extension methods of "StatementSelect"
    /// </summary>
    public static class StatementSelectExtensions
    {

        /// <summary>
        /// Set "Distinct" statement
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Distinct<TModelFrom>(this SqlBinderSelect<TModelFrom> sqlBinderSelect) where TModelFrom : IModelSelectable
        {
            sqlBinderSelect.Distinct = true;
            return sqlBinderSelect;
        }


        #region Join

        /// <summary>
        /// Add Left Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> LeftJoin<TModelFrom, TModelToJoin, TModelJoinedOne>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.LeftJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Left Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> LeftJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, bool>> expressionCondition) 
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable where TModelJoinedTwo : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.LeftJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Left Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <typeparam name="TModelJoinedThree"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> LeftJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable where TModelJoinedTwo : IModelSelectable
            where TModelJoinedThree : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.LeftJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Left Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <typeparam name="TModelJoinedThree"></typeparam>
        /// <typeparam name="TModelJoinedFour"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> LeftJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, TModelJoinedFour>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, TModelJoinedFour, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable where TModelJoinedTwo : IModelSelectable
            where TModelJoinedThree : IModelSelectable where TModelJoinedFour : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.LeftJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }
        
        /// <summary>
        /// Add Inner Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> InnerJoin<TModelFrom, TModelToJoin, TModelJoinedOne>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, bool>> expressionCondition) 
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.InnerJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Inner Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> InnerJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
            where TModelJoinedTwo : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.InnerJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Inner Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <typeparam name="TModelJoinedThree"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> InnerJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
            where TModelJoinedTwo : IModelSelectable where TModelJoinedThree : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.InnerJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Inner Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <typeparam name="TModelJoinedThree"></typeparam>
        /// <typeparam name="TModelJoinedFour"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> InnerJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, TModelJoinedFour>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, TModelJoinedFour, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
            where TModelJoinedTwo : IModelSelectable where TModelJoinedThree : IModelSelectable where TModelJoinedFour : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.InnerJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Right Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> RightJoin<TModelFrom, TModelToJoin, TModelJoinedOne>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, bool>> expressionCondition) 
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.RightJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Right Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> RightJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
            where TModelJoinedTwo : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.RightJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Right Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <typeparam name="TModelJoinedThree"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> RightJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
            where TModelJoinedTwo : IModelSelectable where TModelJoinedThree : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.RightJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Right Join
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelToJoin"></typeparam>
        /// <typeparam name="TModelJoinedOne"></typeparam>
        /// <typeparam name="TModelJoinedTwo"></typeparam>
        /// <typeparam name="TModelJoinedThree"></typeparam>
        /// <typeparam name="TModelJoinedFour"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> RightJoin<TModelFrom, TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, TModelJoinedFour>
            (this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelToJoin, TModelJoinedOne, TModelJoinedTwo, TModelJoinedThree, TModelJoinedFour, bool>> expressionCondition)
            where TModelFrom : IModelSelectable where TModelToJoin : IModelSelectable where TModelJoinedOne : IModelSelectable
            where TModelJoinedTwo : IModelSelectable where TModelJoinedThree : IModelSelectable where TModelJoinedFour : IModelSelectable
        {
            sqlBinderSelect.ListJoins.Add(new JoinCondition()
            {
                TypeJoin = TypeJoin.RightJoin,
                TypeModelToJoin = typeof(TModelToJoin),
                ConditionToJoin = expressionCondition.Body.GetCondition()
            });

            return sqlBinderSelect;
        }

        #endregion


        #region Where

        /// <summary>
        /// Add Where Condition
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TWhere"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Where<TModelFrom, TWhere>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TWhere, bool>> expressionCondition) where TModelFrom : IModelSelectable where TWhere : IModelSelectable
        {
            sqlBinderSelect.WhereCondition =
                sqlBinderSelect.WhereCondition == null ? expressionCondition.Body.GetCondition()
                : new Condition()
                {
                    ConditionLeftNode = sqlBinderSelect.WhereCondition,
                    ConditionOperand = TypeOperandCondition.And,
                    ConditionRightNode = expressionCondition.Body.GetCondition()
                };

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Where Condition
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TWhereOne"></typeparam>
        /// <typeparam name="TWhereTwo"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Where<TModelFrom, TWhereOne, TWhereTwo>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TWhereOne, TWhereTwo, bool>> expressionCondition) where TModelFrom : IModelSelectable where TWhereOne : IModelSelectable where TWhereTwo : IModelSelectable
        {
            sqlBinderSelect.WhereCondition =
                sqlBinderSelect.WhereCondition == null ? expressionCondition.Body.GetCondition()
                : new Condition()
                {
                    ConditionLeftNode = sqlBinderSelect.WhereCondition,
                    ConditionOperand = TypeOperandCondition.And,
                    ConditionRightNode = expressionCondition.Body.GetCondition()
                };

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Where Condition
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TWhereOne"></typeparam>
        /// <typeparam name="TWhereTwo"></typeparam>
        /// <typeparam name="TWhereThree"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Where<TModelFrom, TWhereOne, TWhereTwo, TWhereThree>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TWhereOne, TWhereTwo, TWhereThree, bool>> expressionCondition) where TModelFrom : IModelSelectable where TWhereOne : IModelSelectable where TWhereTwo : IModelSelectable where TWhereThree : IModelSelectable
        {
            sqlBinderSelect.WhereCondition =
                sqlBinderSelect.WhereCondition == null ? expressionCondition.Body.GetCondition()
                : new Condition()
                {
                    ConditionLeftNode = sqlBinderSelect.WhereCondition,
                    ConditionOperand = TypeOperandCondition.And,
                    ConditionRightNode = expressionCondition.Body.GetCondition()
                };

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Where Condition
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TWhereOne"></typeparam>
        /// <typeparam name="TWhereTwo"></typeparam>
        /// <typeparam name="TWhereThree"></typeparam>
        /// <typeparam name="TWhereFour"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Where<TModelFrom, TWhereOne, TWhereTwo, TWhereThree, TWhereFour>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TWhereOne, TWhereTwo, TWhereThree, TWhereFour, bool>> expressionCondition) where TModelFrom : IModelSelectable where TWhereOne : IModelSelectable where TWhereTwo : IModelSelectable where TWhereThree : IModelSelectable where TWhereFour : IModelSelectable
        {
            sqlBinderSelect.WhereCondition =
                sqlBinderSelect.WhereCondition == null ? expressionCondition.Body.GetCondition()
                : new Condition()
                {
                    ConditionLeftNode = sqlBinderSelect.WhereCondition,
                    ConditionOperand = TypeOperandCondition.And,
                    ConditionRightNode = expressionCondition.Body.GetCondition()
                };

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Where Condition
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TWhereOne"></typeparam>
        /// <typeparam name="TWhereTwo"></typeparam>
        /// <typeparam name="TWhereThree"></typeparam>
        /// <typeparam name="TWhereFour"></typeparam>
        /// <typeparam name="TWhereFive"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionCondition"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Where<TModelFrom, TWhereOne, TWhereTwo, TWhereThree, TWhereFour, TWhereFive>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TWhereOne, TWhereTwo, TWhereThree, TWhereFour, TWhereFive, bool>> expressionCondition) where TModelFrom : IModelSelectable where TWhereOne : IModelSelectable where TWhereTwo : IModelSelectable where TWhereThree : IModelSelectable where TWhereFour : IModelSelectable where TWhereFive : IModelSelectable
        {
            sqlBinderSelect.WhereCondition =
                sqlBinderSelect.WhereCondition == null ? expressionCondition.Body.GetCondition()
                : new Condition()
                {
                    ConditionLeftNode = sqlBinderSelect.WhereCondition,
                    ConditionOperand = TypeOperandCondition.And,
                    ConditionRightNode = expressionCondition.Body.GetCondition()
                };

            return sqlBinderSelect;
        }

        #endregion


        #region OrderBy

        /// <summary>
        /// Add Order By ASC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrder"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByAsc<TModelFrom, TModelOrder>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrder, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrder : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByAscending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By ASC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByAsc<TModelFrom, TModelOrderOne, TModelOrderTwo>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByAscending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By ASC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <typeparam name="TModelOrderThree"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByAsc<TModelFrom, TModelOrderOne, TModelOrderTwo, TModelOrderThree>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, TModelOrderThree, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable where TModelOrderThree : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByAscending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By ASC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <typeparam name="TModelOrderThree"></typeparam>
        /// <typeparam name="TModelOrderFour"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByAsc<TModelFrom, TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable where TModelOrderThree : IModelSelectable where TModelOrderFour : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByAscending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By ASC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <typeparam name="TModelOrderThree"></typeparam>
        /// <typeparam name="TModelOrderFour"></typeparam>
        /// <typeparam name="TModelOrderFive"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByAsc<TModelFrom, TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour, TModelOrderFive>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour, TModelOrderFive, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable where TModelOrderThree : IModelSelectable where TModelOrderFour : IModelSelectable where TModelOrderFive : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByAscending
                });
            }

            return sqlBinderSelect;
        }



        /// <summary>
        /// Add Order By DESC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrder"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByDesc<TModelFrom, TModelOrder>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrder, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrder : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByDescending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By DESC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByDesc<TModelFrom, TModelOrderOne, TModelOrderTwo>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByDescending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By DESC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <typeparam name="TModelOrderThree"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByDesc<TModelFrom, TModelOrderOne, TModelOrderTwo, TModelOrderThree>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, TModelOrderThree, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable where TModelOrderThree : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByDescending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By DESC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <typeparam name="TModelOrderThree"></typeparam>
        /// <typeparam name="TModelOrderFour"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByDesc<TModelFrom, TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable where TModelOrderThree : IModelSelectable where TModelOrderFour : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByDescending
                });
            }

            return sqlBinderSelect;
        }

        /// <summary>
        /// Add Order By DESC
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <typeparam name="TModelOrderOne"></typeparam>
        /// <typeparam name="TModelOrderTwo"></typeparam>
        /// <typeparam name="TModelOrderThree"></typeparam>
        /// <typeparam name="TModelOrderFour"></typeparam>
        /// <typeparam name="TModelOrderFive"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="expressionSelectors"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> OrderByDesc<TModelFrom, TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour, TModelOrderFive>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, Expression<Func<TModelOrderOne, TModelOrderTwo, TModelOrderThree, TModelOrderFour, TModelOrderFive, object[]>> expressionSelectors) where TModelFrom : IModelSelectable where TModelOrderOne : IModelSelectable where TModelOrderTwo : IModelSelectable where TModelOrderThree : IModelSelectable where TModelOrderFour : IModelSelectable where TModelOrderFive : IModelSelectable
        {
            List<PropertyInfo> listProperty = expressionSelectors.Body.GetPropertiesFromExpression();
            foreach (PropertyInfo property in listProperty)
            {
                sqlBinderSelect.ListOrderBy.Add(new OrderByInfo()
                {
                    Property = property,
                    TypeOrderBy = TypeOrderBy.OrderByDescending
                });
            }

            return sqlBinderSelect;
        }

        #endregion


        /// <summary>
        /// Set Limit
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Limit<TModelFrom>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, int? limit) where TModelFrom : IModelSelectable
        {
            sqlBinderSelect.Limit = limit;
            return sqlBinderSelect;
        }


        /// <summary>
        /// Set Offset
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static SqlBinderSelect<TModelFrom> Offset<TModelFrom>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, int? offset) where TModelFrom : IModelSelectable
        {
            sqlBinderSelect.Offset = offset;
            return sqlBinderSelect;
        }


        /// <summary>
        /// Bind Sql and execute command
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="connection"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static IEnumerable<TModelFrom> BindAndExecuteGetIEnumerableT<TModelFrom>(this SqlBinderSelect<TModelFrom> sqlBinderSelect, IDbConnection connection, Dictionary<string, object> parameters = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelFrom : IModelSelectable
        {
            return connection.GetAdapter().BindAndExecuteGetListT(sqlBinderSelect, connection, parameters, transaction, commandTimeout, commandType);
        }

    }

}
