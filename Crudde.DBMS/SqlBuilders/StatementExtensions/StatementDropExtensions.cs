﻿using System.Data;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Static class with extension methods of "StatementDrop"
    /// </summary>
    public static class StatementDropExtensions
    {

        #region Table


        /// <summary>
        /// Add Check "IF EXIST"
        /// </summary>
        /// <param name="sqlBinderDropTable"></param>
        /// <returns></returns>
        public static SqlBinderDropTable<TModelToDrop> IfExist<TModelToDrop>(this SqlBinderDropTable<TModelToDrop> sqlBinderDropTable) where TModelToDrop : ICruddeTable
        {
            sqlBinderDropTable.CheckIfExist = true;
            return sqlBinderDropTable;
        }


        /// <summary>
        /// Add Drop in Cascade
        /// </summary>
        /// <param name="sqlBinderDropTable"></param>
        /// <returns></returns>
        public static SqlBinderDropTable<TModelToDrop> Cascade<TModelToDrop>(this SqlBinderDropTable<TModelToDrop> sqlBinderDropTable) where TModelToDrop : ICruddeTable
        {
            sqlBinderDropTable.DropCascade = true;
            return sqlBinderDropTable;
        }


        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <param name="sqlBinderDropTable"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        public static void BindAndExecute<TModelToDrop>(this SqlBinderDropTable<TModelToDrop> sqlBinderDropTable, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDrop : ICruddeTable
        {
            connection.GetAdapter().BindAndExecute(sqlBinderDropTable, connection, transaction, commandTimeout, commandType);
        }


        #endregion


        #region View


        /// <summary>
        /// Add Check "IF EXIST"
        /// </summary>
        /// <param name="sqlBinderDropView"></param>
        /// <returns></returns>
        public static SqlBinderDropView<TModelToDrop> IfExist<TModelToDrop>(this SqlBinderDropView<TModelToDrop> sqlBinderDropView) where TModelToDrop : ICruddeView
        {
            sqlBinderDropView.CheckIfExist = true;
            return sqlBinderDropView;
        }


        /// <summary>
        /// Add Drop in Cascade
        /// </summary>
        /// <param name="sqlBinderDropView"></param>
        /// <returns></returns>
        public static SqlBinderDropView<TModelToDrop> Cascade<TModelToDrop>(this SqlBinderDropView<TModelToDrop> sqlBinderDropView) where TModelToDrop : ICruddeView
        {
            sqlBinderDropView.DropCascade = true;
            return sqlBinderDropView;
        }


        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <param name="sqlBinderDropView"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        public static void BindAndExecute<TModelToDrop>(this SqlBinderDropView<TModelToDrop> sqlBinderDropView, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDrop : ICruddeView
        {
            connection.GetAdapter().BindAndExecute(sqlBinderDropView, connection, transaction, commandTimeout, commandType);
        }


        #endregion

    }

}
