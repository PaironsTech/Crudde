﻿using System.Data;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Static class with extension methods of "StatementInsert"
    /// </summary>
    public static class StatementInsertExtensions
    {

        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static int BindAndExecuteGetInt<TModelToInsert>(this SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable
        {
            return connection.GetAdapter().BindAndExecuteGetInt(sqlBinderInsert, connection, transaction, commandTimeout, commandType);
        }

        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static object BindAndExecuteGetObject<TModelToInsert>(this SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable
        {
            return connection.GetAdapter().BindAndExecuteGetObject(sqlBinderInsert, connection, transaction, commandTimeout, commandType);
        }

        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static TModelToInsert BindAndExecuteGetTModel<TModelToInsert>(this SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable
        {
            return connection.GetAdapter().BindAndExecuteGetTModel(sqlBinderInsert, connection, transaction, commandTimeout, commandType);
        }

    }

}
