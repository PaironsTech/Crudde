﻿using System.Collections.Generic;
using System.Data;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Static class with extension methods of "StatementDelete"
    /// </summary>
    public static class StatementDeleteExtensions
    {

        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToDelete"></typeparam>
        /// <param name="sqlBinderDelete"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns>Bool that check if the operation work</returns>
        public static bool BindAndExecuteGetBool<TModelToDelete>(this SqlBinderDelete<TModelToDelete> sqlBinderDelete, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDelete : IModelDeletable
        {
            return connection.GetAdapter().BindAndExecuteGetBool(sqlBinderDelete, connection, transaction, commandTimeout, commandType);
        }


        /// <summary>
        /// Bind the Sql and execute the command.
        /// </summary>
        /// <typeparam name="TModelToDelete"></typeparam>
        /// <param name="sqlBinderDelete"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns>List of TModelToDelete with information of rows deleted</returns>
        public static IEnumerable<TModelToDelete> BindAndExecuteGetIEnumerableT<TModelToDelete>(this SqlBinderDelete<TModelToDelete> sqlBinderDelete, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDelete : IModelDeletable
        {
            return connection.GetAdapter().BindAndExecuteGetTModel(sqlBinderDelete, connection, transaction, commandTimeout, commandType);
        }

    }
}
