﻿using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.SqlBinders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Partial reference to Static class "SqlBuilder"
    /// </summary>
    public static partial class SqlBuilder
    {

        /// <summary>
        /// Static class of Sql "Insert" command
        /// </summary>
        public static class StatementInsert
        {

            /// <summary>
            /// Method of First Level for Insert Data
            /// </summary>
            /// <typeparam name="TModelToInsert"></typeparam>
            /// <param name="modelToInsert"></param>
            /// <returns></returns>
            public static SqlBinderInsert<TModelToInsert> Insert<TModelToInsert>(TModelToInsert modelToInsert) where TModelToInsert : IModelInsertable
            {
                Type modelType = typeof(TModelToInsert);

                // Execute Control
                PropertyInfo keyProperty = modelType.GetTypeKeyOrExplicitKeyProperty();
                if (keyProperty == null) throw new ArgumentException("Entity must have at least one [Key] or [ExplicitKey] property");

                // Create Dictionary for Column and values
                Dictionary<PropertyInfo, object> columnsValues = new Dictionary<PropertyInfo, object>();

                // Add Elements to dictionary
                modelType.GetTypeProperties().Except(
                    modelType.GetTypeForeignKeyMappedProperties().Union(
                        modelType.GetTypeIgnoreInInsertProperties().Union(
                            modelType.GetTypeKeyProperties().Union(
                                modelType.GetTypeNotWriteProperties()
                            )
                        )
                    )
                )
                .ToList()
                .ForEach(f => columnsValues.Add(f, f.GetValue(modelToInsert)));

                // Return Binder
                return new SqlBinderInsert<TModelToInsert>()
                {
                    PrimaryKey = keyProperty,
                    ColumnsValues = columnsValues
                };
            }

        }

    }

}
