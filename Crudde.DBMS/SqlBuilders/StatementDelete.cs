﻿using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.SqlBinders;
using System;
using System.Reflection;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Partial reference to Static class "SqlBuilder"
    /// </summary>
    public static partial class SqlBuilder
    {

        /// <summary>
        /// Static class of Sql "Delete" command
        /// </summary>
        public static class StatementDelete
        {

            /// <summary>
            /// Method of first level for Delete rows in Database
            /// </summary>
            /// <typeparam name="TModelToDelete"></typeparam>
            /// <param name="modelToDelete"></param>
            /// <returns></returns>
            public static SqlBinderDelete<TModelToDelete> Delete<TModelToDelete>(TModelToDelete modelToDelete) where TModelToDelete : IModelDeletable
            {
                // Execute Control
                PropertyInfo keyProperty = typeof(TModelToDelete).GetTypeKeyOrExplicitKeyProperty();
                if (keyProperty == null) throw new ArgumentException("Entity must have at least one [Key] or [ExplicitKey] property");

                // Return Binder
                return new SqlBinderDelete<TModelToDelete>()
                {
                    WhereCondition = new Condition()
                    {
                        ConditionLeftNode = new Condition()
                        {
                            IsNode = true,
                            NodeConditionType = TypeNodeCondition.Property,
                            NodeConditionProperty = keyProperty
                        },
                        ConditionOperand = TypeOperandCondition.Equal,
                        ConditionRightNode = new Condition()
                        {
                            IsNode = true,
                            NodeConditionType = TypeNodeCondition.Constant,
                            NodeConditionValue = keyProperty.GetValue(modelToDelete)
                        }
                    }
                };
            }

        }

    }

}
