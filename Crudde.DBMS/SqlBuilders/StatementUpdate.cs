﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Partial reference to Static class "SqlBuilder"
    /// </summary>
    public static partial class SqlBuilder
    {

        /// <summary>
        /// Static class of Sql "Update" command
        /// </summary>
        public static class StatementUpdate
        {

            /// <summary>
            /// Method of First Level to Update Data
            /// </summary>
            /// <typeparam name="TModelToUpdate"></typeparam>
            /// <param name="modelToUpdate"></param>
            /// <returns></returns>
            public static SqlBinderUpdate<TModelToUpdate> Update<TModelToUpdate>(TModelToUpdate modelToUpdate) where TModelToUpdate : IModelUpdatable
            {
                Type modelType = typeof(TModelToUpdate);

                // Execute Control
                PropertyInfo keyProperty = modelType.GetTypeKeyOrExplicitKeyProperty();
                if (keyProperty == null) throw new ArgumentException("Entity must have at least one [Key] or [ExplicitKey] property");

                // Create Dictionary for Column and values
                Dictionary<PropertyInfo, object> columnsValues = new Dictionary<PropertyInfo, object>();

                // Add Elements to dictionary
                modelType.GetTypeProperties().Except(
                    modelType.GetTypeForeignKeyMappedProperties().Union(
                        modelType.GetTypeIgnoreInUpdateProperties().Union(
                            modelType.GetTypeKeyProperties().Union(
                                modelType.GetTypeNotWriteProperties()
                            )
                        )
                    )
                )
                .ToList()
                .ForEach(f => columnsValues.Add(f, f.GetValue(modelToUpdate)));


                // Return Binder
                return new SqlBinderUpdate<TModelToUpdate>()
                {
                    ColumnsValues = columnsValues,
                    WhereClause = new Condition()
                    {
                        ConditionLeftNode = new Condition()
                        {
                            IsNode = true,
                            NodeConditionType = TypeNodeCondition.Property,
                            NodeConditionProperty = keyProperty
                        },
                        ConditionOperand = TypeOperandCondition.Equal,
                        ConditionRightNode = new Condition()
                        {
                            IsNode = true,
                            NodeConditionType = TypeNodeCondition.Constant,
                            NodeConditionValue = keyProperty.GetValue(modelToUpdate)
                        }
                    }

                };
            }

        }

    }

}
