﻿using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Partial reference to Static class "SqlBuilder"
    /// </summary>
    public static partial class SqlBuilder
    {

        /// <summary>
        /// Static class of Sql "Select" command
        /// </summary>
        public static class StatementSelect
        {

            /// <summary>
            /// Method of First Level for Select Data
            /// </summary>
            /// <typeparam name="TModelFrom"></typeparam>
            /// <returns></returns>
            public static SqlBinderSelect<TModelFrom> From<TModelFrom>() where TModelFrom : IModelSelectable
            {
                return new SqlBinderSelect<TModelFrom>();
            }

        }

    }

}
