﻿using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlBuilders
{

    /// <summary>
    /// Partial reference to Static class "SqlBuilder"
    /// </summary>
    public static partial class SqlBuilder
    {

        /// <summary>
        /// Static class of Sql "Drop" command
        /// </summary>
        public static class StatementDrop
        {

            /// <summary>
            /// Method of First Level for Drop Table
            /// </summary>
            /// <typeparam name="TModelToDrop"></typeparam>
            /// <returns></returns>
            public static SqlBinderDropTable<TModelToDrop> Table<TModelToDrop>() where TModelToDrop : ICruddeTable
            {
                return new SqlBinderDropTable<TModelToDrop>();
            }


            /// <summary>
            /// Method of First Level for Drop View
            /// </summary>
            /// <typeparam name="TModelToDrop"></typeparam>
            /// <returns></returns>
            public static SqlBinderDropView<TModelToDrop> View<TModelToDrop>() where TModelToDrop : ICruddeView
            {
                return new SqlBinderDropView<TModelToDrop>();
            }

        }

    }

}
