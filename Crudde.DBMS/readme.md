# Dapper.DBMS

Dapper.DBMS contains all feature that improve Database mapping like:

* Custom attributes for models and their properties.
* Caching of queries and Model-Properties-Types
* Database Adapter.
* Extension about some objects and Proxy Mapper.

## Attributes

### TableAttribute 

It allows to set the correct name of the table in Db and a view associated where you do the Select.

Example:

```

[Table("TestTable", "TestTableView")]
class Test 
{
    // The name of view is optional, if itìs empty it will not considered.
}

```


### ColumnAttribute

It allows to set the correct name of the column in Db.

Example:

```

class Test 
{
    [Column("CorrectNameColumn")]
    public string CustomColumn { get; set; }
}

```


### KeyAttribute 

Specifies that this field is a primary key in the database and is auto-generated.<br />
If the Key isn't setting the mapper search the property named "id".

Example:

```

class Test 
{
    [Key]
    public long Id { get; set; }
}

```


### ExplicitKeyAttribute 

That rappresent the columns that are Primary Key but aren't auto-generated from Db.

Example:

```

class Test 
{
    [ExplicitKey]
    public Guid UserIDCode { get; set; }
}

```


### ComputedAttribute 

That rappresent the columns that are created by Db and must be mapped during Select, but not in Insert and Update. <br />
It was expanded with enum that rappresent the type of computed column. This is useful for SqlBuilder.

Example:

```

class Test 
{
    [Computed(TypeComputed.COUNT)]
    public int CountRecord { get; set; }
}

```


### IgnoreInInsertAttribute 

Specifies that this column have to be ignored on Insert methods.

Example:

```

class Test 
{
    [IgnoreInInsert]
    public DateTime? LastUpdateRecord { get; set; }
}

```


### IgnoreInUpdateAttribute 

Specifies that this column have to be ignored on Update methods.

Example:

```

class Test 
{
    [IgnoreInUpdate]
    public DateTime DateTimeCreationRecord { get; set; }
}

```


### ForeignKeyMappingObjectAttribute

Specifies that this not a column of table, but an object that rappresent a model that we have, in table, the foreign key.<br />
It's useful for multi-mapping and joins.<br />
We passing as parameter the name of the property it's the foreign key associated.

Example:

```

class Test 
{
    [Key]
    public long Id { get; set; }
    public long IdUser { get; set; }
	
    [ForeignKeyMappingObject("IdUser")]
    public User UserFK { get; set; }
}

```



## Caching

Caching is made by ConcurrentDictionary that save all properties of Models used and divide it based on type of attributes.


## Database Adapter

There is an abstract class SqlAdapter that contains the virtual methods that must be override in every Database adapter.

List of Database supported:

* FireBird
* MySql
* Postgresql
* SQL Lite
* SQL Server 
* SQL Server Compact Edition

List of Method:

* AppendColumnName(StringBuilder sb, string columnName) <br />Add the correct format of column name.
* AppendColumnNameEqualsValue(StringBuilder sb, string columnName)<br />Adds a column equality to a parameter in the correct format.
* GetTableNameFormat(string tableName)<br />Return the table name in the correct format.
* GetColumnNameFormat(string columnName)<br />Return the column name in the correct format.