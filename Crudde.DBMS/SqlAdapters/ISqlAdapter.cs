﻿using System.Collections.Generic;
using System.Data;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlAdapters
{

    /// <summary>
    /// The interface for all database adapters
    /// </summary>
    internal interface ISqlAdapter
    {

        #region Bind Select


        /// <summary>
        /// Bind SqlBinderSelect object and return List of <typeparamref name="TModelFrom"/>
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="connection"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        IEnumerable<TModelFrom> BindAndExecuteGetListT<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect, IDbConnection connection, Dictionary<string, object> parameters = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelFrom : IModelSelectable;


        #endregion


        #region Bind Insert


        /// <summary>
        /// Bind SqlBinderInsert object and return int that rappresent the number of rows Edited
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        int BindAndExecuteGetInt<TModelToInsert>(SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable;

        /// <summary>
        /// Bind SqlBinderInsert object and return the primary key inserted
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        object BindAndExecuteGetObject<TModelToInsert>(SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable;

        /// <summary>
        /// Bind SqlBinderInsert object and return the <typeparamref name="TModelToInsert"/> inserted
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        TModelToInsert BindAndExecuteGetTModel<TModelToInsert>(SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable;


        #endregion


        #region Bind Update


        /// <summary>
        /// Bind SqlBinderUpdate object and return true if the update command worked
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        bool BindAndExecuteGetBool<TModelToUpdate>(SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable;

        /// <summary>
        /// Bind SqlBinderInsert object and return int that rappresent the number of rows updated
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        int BindAndExecuteGetInt<TModelToUpdate>(SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable;

        /// <summary>
        /// Bind SqlBinderInsert object and return a list of <typeparamref name="TModelToUpdate"/> updated
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        IEnumerable<TModelToUpdate> BindAndExecuteGetTModel<TModelToUpdate>(SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable;


        #endregion


        #region Bind Delete


        /// <summary>
        /// Bind SqlBinderDelete object and return true if Delete command worked
        /// </summary>
        /// <typeparam name="TModelToDelete"></typeparam>
        /// <param name="sqlBinderDelete"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        bool BindAndExecuteGetBool<TModelToDelete>(SqlBinderDelete<TModelToDelete> sqlBinderDelete, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDelete : IModelDeletable;

        /// <summary>
        /// Bind SqlBinderDelete object and return list of <typeparamref name="TModelToDelete"/> deleted
        /// </summary>
        /// <typeparam name="TModelToDelete"></typeparam>
        /// <param name="sqlBinderDelete"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        IEnumerable<TModelToDelete> BindAndExecuteGetTModel<TModelToDelete>(SqlBinderDelete<TModelToDelete> sqlBinderDelete, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDelete : IModelDeletable;


        #endregion


        #region Bind Create

        // TO DO 

        #endregion


        #region Bind Alter

        // TO DO 

        #endregion


        #region Bind Drop


        /// <summary>
        /// Bind SqlBinderDropTables object and return true if the drop command worked
        /// </summary>
        /// <param name="sqlBinderDropTable"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        void BindAndExecute<TModelToDrop>(SqlBinderDropTable<TModelToDrop> sqlBinderDropTable, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDrop : ICruddeTable;

        /// <summary>
        /// Bind SqlBinderDropViews object and return true if the drop command worked
        /// </summary>
        /// <param name="sqlBinderDropView"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        void BindAndExecute<TModelToDrop>(SqlBinderDropView<TModelToDrop> sqlBinderDropView, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDrop : ICruddeView;


        #endregion


        #region Bind Sql Commands


        /// <summary>
        /// Bind Operand Condition Format
        /// </summary>
        /// <param name="typeOperandCondition"></param>
        /// <param name="prefixConstantValue"></param>
        /// <param name="postfixConstantValue"></param>
        /// <returns></returns>
        string BindOperandCondition(TypeOperandCondition typeOperandCondition, out string prefixConstantValue, out string postfixConstantValue);

        /// <summary>
        /// Bind Table Name Format
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        string BindTableName(string tableName);

        /// <summary>
        /// Bind Column Name Format
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        string BindColumnName(string columnName);


        #endregion

    }
}
