﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.Helpers.SqlMapping;
using PaironsTech.Crudde.DBMS.Helpers.SqlParameters;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.SqlAdapters
{

    /// <summary>
    /// The Postgres database adapter.
    /// </summary>
    internal class SqlAdapterPostgres : ISqlAdapter
    {

        #region Bind Select


        /// <summary>
        /// Bind SqlBinderSelect object and return List of <typeparamref name="TModelFrom"/>
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="connection"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public IEnumerable<TModelFrom> BindAndExecuteGetListT<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect, IDbConnection connection, Dictionary<string, object> parameters = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelFrom : IModelSelectable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            // Adding external parameters to query Params
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    queryParams.Add(param.Key, param.Value);
                }
            }

            // Create the Sql
            var sbSql = new StringBuilder();
            sbSql.AppendFormat("SELECT " + (sqlBinderSelect.Distinct ? "DISTINCT" : string.Empty) + " {0} ", "*"); // TO DO : Select only some fields !
            sbSql.AppendFormat("FROM {0} ", BindTableName(typeof(TModelFrom).GetTrueModelName()));

            if (sqlBinderSelect.ListJoins != null && sqlBinderSelect.ListJoins.Count != 0)
                foreach (JoinCondition join in sqlBinderSelect.ListJoins)
                    sbSql.AppendFormat(
                        (join.TypeJoin == TypeJoin.InnerJoin ? "INNER" : join.TypeJoin == TypeJoin.LeftJoin ? "LEFT" : "RIGHT")
                        + " JOIN " + BindTableName(join.TypeModelToJoin.GetTrueModelName()) + " ON " +
                        join.ConditionToJoin.Bind(this, queryParams, out queryParams) + " "
                    );

            if (sqlBinderSelect.WhereCondition != null)
                sbSql.AppendFormat("WHERE {0} ", sqlBinderSelect.WhereCondition.Bind(this, queryParams, out queryParams));

            if (sqlBinderSelect.ListOrderBy != null && sqlBinderSelect.ListOrderBy.Count != 0)
                sbSql.AppendFormat("ORDER BY {0}", string.Join(", ", sqlBinderSelect.ListOrderBy.Select
                    (
                        s => BindTableName(s.Property.GetPropertyInfoTableName()) + "." + BindColumnName(s.Property.GetPropertyInfoColumnName()) + " " + 
                            (s.TypeOrderBy == TypeOrderBy.OrderByAscending ? "ASC" : "DESC")
                    )) + " ");

            if (sqlBinderSelect.Limit != null)
                sbSql.AppendFormat("LIMIT {0} ", sqlBinderSelect.Limit);

            if (sqlBinderSelect.Offset != null)
                sbSql.AppendFormat("OFFSET {0} ", sqlBinderSelect.Offset);

            return SqlMappingSelect.MapQueryResult(sqlBinderSelect, connection, sbSql, queryParams.ConvertToObject(), transaction, true, commandTimeout, commandType);
        }


        #endregion


        #region Bind Insert

        /// <summary>
        /// Bind SqlBinderInsert object and return int that rappresent the number of rows Edited
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public int BindAndExecuteGetInt<TModelToInsert>(SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            string tableNameFormatted = BindTableName(typeof(TModelToInsert).GetTrueModelName());
            string columnDbNameFormatted = string.Join(", ", sqlBinderInsert.ColumnsValues.Select(s => BindColumnName(s.Key.GetPropertyInfoColumnName())));
            string parametersList = string.Join(", ", sqlBinderInsert.ColumnsValues.Select(s => "@" + SqlParameter.GetNameParam(queryParams, s.Value, out queryParams, s.Key.Name)));

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2})", tableNameFormatted, columnDbNameFormatted, parametersList);

            return connection.Execute(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout, commandType);
        }


        /// <summary>
        /// Bind SqlBinderInsert object and return the primary key inserted
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public object BindAndExecuteGetObject<TModelToInsert>(SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            string tableNameFormatted = BindTableName(typeof(TModelToInsert).GetTrueModelName());
            string columnDbNameFormatted = string.Join(", ", sqlBinderInsert.ColumnsValues.Select(s => BindColumnName(s.Key.GetPropertyInfoColumnName())));
            string parametersList = string.Join(", ", sqlBinderInsert.ColumnsValues.Select(s => "@" + SqlParameter.GetNameParam(queryParams, s.Value, out queryParams, s.Key.Name)));
            string primaryKeyFormatted = BindColumnName(sqlBinderInsert.PrimaryKey.GetPropertyInfoColumnName());

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2}) RETURNING {3}", tableNameFormatted, columnDbNameFormatted, parametersList, primaryKeyFormatted);

            var results = connection.Query(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout: commandTimeout, commandType: commandType).ToList();
            return ((IDictionary<string, object>)results[0])[sqlBinderInsert.PrimaryKey.GetPropertyInfoColumnName()];
        }


        /// <summary>
        /// Bind SqlBinderInsert object and return the <typeparamref name="TModelToInsert"/> inserted
        /// </summary>
        /// <typeparam name="TModelToInsert"></typeparam>
        /// <param name="sqlBinderInsert"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public TModelToInsert BindAndExecuteGetTModel<TModelToInsert>(SqlBinderInsert<TModelToInsert> sqlBinderInsert, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToInsert : IModelInsertable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            string tableNameFormatted = BindTableName(typeof(TModelToInsert).GetTrueModelName());
            string columnDbNameFormatted = string.Join(", ", sqlBinderInsert.ColumnsValues.Select(s => BindColumnName(s.Key.GetPropertyInfoColumnName())));
            string parametersList = string.Join(", ", sqlBinderInsert.ColumnsValues.Select(s => "@" + SqlParameter.GetNameParam(queryParams, s.Value, out queryParams, s.Key.Name)));

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("INSERT INTO {0} ({1}) VALUES ({2}) RETURNING *", tableNameFormatted, columnDbNameFormatted, parametersList);

            List<TModelToInsert> listInserted = connection.Query<TModelToInsert>(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout: commandTimeout, commandType: commandType).ToList();
            if (listInserted.Count > 0) return listInserted[0];

            return default(TModelToInsert);
        }

        #endregion


        #region Bind Update

        /// <summary>
        /// Bind SqlBinderUpdate object and return true if the update command worked
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public bool BindAndExecuteGetBool<TModelToUpdate>(SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            string tableNameFormatted = BindTableName(typeof(TModelToUpdate).GetTrueModelName());
            string setColumnNameValue = string.Join(", ", sqlBinderUpdate.ColumnsValues.Select(s => BindColumnName(s.Key.GetPropertyInfoColumnName()) + " = @" + SqlParameter.GetNameParam(queryParams, s.Value, out queryParams, s.Key.Name)));
            string whereCondition = sqlBinderUpdate.WhereClause.Bind(this, queryParams, out queryParams);

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("UPDATE {0} SET {1} WHERE {2}", tableNameFormatted, setColumnNameValue, whereCondition);

            return connection.Execute(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout, commandType) > 0;
        }


        /// <summary>
        /// Bind SqlBinderInsert object and return int that rappresent the number of rows updated
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public int BindAndExecuteGetInt<TModelToUpdate>(SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            string tableNameFormatted = BindTableName(typeof(TModelToUpdate).GetTrueModelName());
            string setColumnNameValue = string.Join(", ", sqlBinderUpdate.ColumnsValues.Select(s => BindColumnName(s.Key.GetPropertyInfoColumnName()) + " = @" + SqlParameter.GetNameParam(queryParams, s.Value, out queryParams, s.Key.Name)));
            string whereCondition = sqlBinderUpdate.WhereClause.Bind(this, queryParams, out queryParams);

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("UPDATE {0} SET {1} WHERE {2}", tableNameFormatted, setColumnNameValue, whereCondition);

            return connection.Execute(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout, commandType);
        }


        /// <summary>
        /// Bind SqlBinderInsert object and return a list of <typeparamref name="TModelToUpdate"/> updated
        /// </summary>
        /// <typeparam name="TModelToUpdate"></typeparam>
        /// <param name="sqlBinderUpdate"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public IEnumerable<TModelToUpdate> BindAndExecuteGetTModel<TModelToUpdate>(SqlBinderUpdate<TModelToUpdate> sqlBinderUpdate, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToUpdate : IModelUpdatable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();

            string tableNameFormatted = BindTableName(typeof(TModelToUpdate).GetTrueModelName());
            string setColumnNameValue = string.Join(", ", sqlBinderUpdate.ColumnsValues.Select(s => BindColumnName(s.Key.GetPropertyInfoColumnName()) + " = @" + SqlParameter.GetNameParam(queryParams, s.Value, out queryParams, s.Key.Name)));
            string whereCondition = sqlBinderUpdate.WhereClause.Bind(this, queryParams, out queryParams);

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("UPDATE {0} SET {1} WHERE {2} RETURNING *", tableNameFormatted, setColumnNameValue, whereCondition);

            return connection.Query<TModelToUpdate>(sbSql.ToString(), queryParams.ConvertToObject(), transaction: transaction, commandTimeout: commandTimeout, commandType: commandType);
        }

        #endregion


        #region Bind Delete

        /// <summary>
        /// Bind SqlBinderDelete object and return true if Delete command worked
        /// </summary>
        /// <typeparam name="TModelToDelete"></typeparam>
        /// <param name="sqlBinderDelete"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public bool BindAndExecuteGetBool<TModelToDelete>(SqlBinderDelete<TModelToDelete> sqlBinderDelete, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDelete : IModelDeletable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();
            string tableNameFormatted = BindTableName(typeof(TModelToDelete).GetTrueModelName());

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("DELETE FROM {0} ", tableNameFormatted);

            if (sqlBinderDelete.WhereCondition != null)
                sbSql.AppendFormat("WHERE {0} ", sqlBinderDelete.WhereCondition.Bind(this, queryParams, out queryParams));

            return connection.Execute(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout, commandType) > 0;
        }


        /// <summary>
        /// Bind SqlBinderDelete object and return list of <typeparamref name="TModelToDelete"/> deleted
        /// </summary>
        /// <typeparam name="TModelToDelete"></typeparam>
        /// <param name="sqlBinderDelete"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public IEnumerable<TModelToDelete> BindAndExecuteGetTModel<TModelToDelete>(SqlBinderDelete<TModelToDelete> sqlBinderDelete, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDelete : IModelDeletable
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();
            string tableNameFormatted = BindTableName(typeof(TModelToDelete).GetTrueModelName());

            var sbSql = new StringBuilder();
            sbSql.AppendFormat("DELETE FROM {0} ", tableNameFormatted);

            if (sqlBinderDelete.WhereCondition != null)
                sbSql.AppendFormat("WHERE {0} RETURNING *", sqlBinderDelete.WhereCondition.Bind(this, queryParams, out queryParams));

            return connection.Query<TModelToDelete>(sbSql.ToString(), queryParams.ConvertToObject(), transaction, commandTimeout: commandTimeout, commandType: commandType);
        }

        #endregion


        #region Bind Create

        // TO DO 

        #endregion


        #region Bind Alter

        // TO DO 

        #endregion


        #region Bind Drop


        /// <summary>
        /// Bind SqlBinderDropTables object and return true if the drop command worked
        /// </summary>
        /// <param name="sqlBinderDropTable"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        public void BindAndExecute<TModelToDrop>(SqlBinderDropTable<TModelToDrop> sqlBinderDropTable, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDrop : ICruddeTable
        {
            StringBuilder sbSql = new StringBuilder("DROP TABLE ");

            if (sqlBinderDropTable.CheckIfExist)
                sbSql.Append("IF EXISTS ");

            sbSql.Append(BindTableName(typeof(TModelToDrop).GetTrueModelName()) + " ");
            sbSql.Append(sqlBinderDropTable.DropCascade ? "CASCADE" : "RESTRICT");

            connection.Execute(sbSql.ToString(), param: null, transaction: transaction, commandTimeout: commandTimeout, commandType: commandType);
        }

        /// <summary>
        /// Bind SqlBinderDropViews object and return true if the drop command worked
        /// </summary>
        /// <param name="sqlBinderDropView"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        public void BindAndExecute<TModelToDrop>(SqlBinderDropView<TModelToDrop> sqlBinderDropView, IDbConnection connection, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null) where TModelToDrop : ICruddeView
        {
            StringBuilder sbSql = new StringBuilder("DROP VIEW ");

            if (sqlBinderDropView.CheckIfExist)
                sbSql.Append("IF EXISTS ");

            sbSql.Append(BindTableName(typeof(TModelToDrop).GetTrueModelName()));
            sbSql.Append(sqlBinderDropView.DropCascade ? "CASCADE" : "RESTRICT");

            connection.Execute(sbSql.ToString(), param: null, transaction: transaction, commandTimeout: commandTimeout, commandType: commandType);
        }


        #endregion


        #region Bind Sql Commands


        /// <summary>
        /// Bind Operand Condition Format
        /// </summary>
        /// <param name="typeOperandCondition"></param>
        /// <param name="prefixConstantValue"></param>
        /// <param name="postfixConstantValue"></param>
        /// <returns></returns>
        public string BindOperandCondition(TypeOperandCondition typeOperandCondition, out string prefixConstantValue, out string postfixConstantValue)
        {
            switch(typeOperandCondition)
            {
                case TypeOperandCondition.And:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "AND";

                case TypeOperandCondition.Equal:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "=";

                case TypeOperandCondition.GreaterThan:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return ">";

                case TypeOperandCondition.GreaterThanOrEqual:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return ">=";

                case TypeOperandCondition.LessThan:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "<";

                case TypeOperandCondition.LessThanOrEqual:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "<=";

                case TypeOperandCondition.NotEqual:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "<>";

                case TypeOperandCondition.Or:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "OR";

                case TypeOperandCondition.Like:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "LIKE";

                case TypeOperandCondition.ILike:
                    prefixConstantValue = string.Empty;
                    postfixConstantValue = string.Empty;
                    return "ILIKE";

                case TypeOperandCondition.In:
                    prefixConstantValue = "(";
                    postfixConstantValue = ")";
                    return "= ANY";

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Bind Table Name Format
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public string BindTableName(string tableName)
        {
            return "\"" + tableName + "\"";
        }

        /// <summary>
        /// Bind Column Name Format
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public string BindColumnName(string columnName)
        {
            return "\"" + columnName + "\"";
        }


        #endregion
        
    }
}
