﻿using System;

namespace PaironsTech.Crudde.DBMS.Attributes
{

    /// <summary>
    /// Defines the name of a table to use in Dapper.DBMS commands.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {

        /// <summary>
        /// The name of the table in the database
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Constructor of Table Attribute passing the name of the table
        /// </summary>
        /// <param name="tableName">The name of this table in the database.</param>
        public TableAttribute(string tableName)
        {
            Name = tableName;
        }

    }
}
