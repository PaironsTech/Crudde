﻿using System;

namespace PaironsTech.Crudde.DBMS.Attributes
{

    /// <summary>
    /// Defines the name of a view to use in Dapper.DBMS commands.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ViewAttribute : Attribute
    {

        /// <summary>
        /// The name of the view in the database
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Constructor of View Attribute passing the name of the view
        /// </summary>
        /// <param name="viewName">The name of this table in the database.</param>
        public ViewAttribute(string viewName)
        {
            Name = viewName;
        }

    }
}
