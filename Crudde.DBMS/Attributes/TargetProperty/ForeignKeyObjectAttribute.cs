﻿using System;

namespace PaironsTech.Crudde.DBMS.Attributes
{

    /// <summary>
    /// Define the property as Foreign Key Object for auto-mapping and reference the forein key with ID associated
    /// </summary>
    public class ForeignKeyObjectAttribute : Attribute
    {

        /// <summary>
        /// Constructor of Foreign Key Mapped Attribute passing the name of the key associated
        /// </summary>
        public ForeignKeyObjectAttribute() { }

    }
}
