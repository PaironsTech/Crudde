﻿using System;

namespace PaironsTech.Crudde.DBMS.Attributes
{

    /// <summary>
    /// Defines the name of a column to use in Dapper.Contrib commands
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {

        /// <summary>
        /// The name of the column in the database
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Constructor of Column Attribute passing the name of the column name in db.
        /// </summary>
        /// <param name="columnName">The name of this column in the database.</param>
        public ColumnAttribute(string columnName)
        {
            Name = columnName;
        }

    }
}
