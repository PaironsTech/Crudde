﻿using System;

namespace PaironsTech.Crudde.DBMS.Attributes
{

    /// <summary>
    /// Specifies that this column have to be ignored on Update execution
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreInUpdateAttribute : Attribute { }

}
