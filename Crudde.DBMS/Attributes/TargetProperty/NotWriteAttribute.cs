﻿using System;

namespace PaironsTech.Crudde.DBMS.Attributes
{

    /// <summary>
    /// Specifies that the field is not writable in the database.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotWriteAttribute : Attribute
    {

        /// <summary>
        /// Specifies that the property is not writable in the database.
        /// </summary>
        public NotWriteAttribute() { }

    }
}
