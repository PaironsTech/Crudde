﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlMapping
{

    /// <summary>
    /// Support object for mapping objects returned from query
    /// </summary>
    public class SqlMapSelSupFKMapped
    {

        /// <summary>
        /// Property Info that have the Foreign Key Mapped Attribute
        /// </summary>
        public PropertyInfo PropertyInfo { get; }

        /// <summary>
        /// The Type of the object of the Property
        /// </summary>
        public Type TypeProperty { get; }

        /// <summary>
        /// The Type of the Primary Key of the Declaring Type of the Property
        /// </summary>
        public Type TypePrimaryKey { get; }
        
        /// <summary>
        /// A Dictionary used for mapping one to many relationship. 
        /// It's key value with TypePrimaryKey and Declaring Type of the Property Info
        /// </summary>
        public object DictionaryOneToMany { get; }



        /// <summary>
        /// Constructor of 'SqlMapSelSupFKMapped'
        /// </summary>
        /// <param name="propertyInfo"></param>
        public SqlMapSelSupFKMapped(PropertyInfo propertyInfo)
        {
            PropertyInfo = propertyInfo;

            if (PropertyInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                TypeProperty = PropertyInfo.PropertyType.GetGenericArguments()[0];
            }
            else
            {
                TypeProperty = PropertyInfo.PropertyType;
            }

            TypePrimaryKey = TypeProperty.GetTypeKeyOrExplicitKeyProperty().PropertyType;
            DictionaryOneToMany = Activator.CreateInstance(typeof(Dictionary<,>).MakeGenericType(TypePrimaryKey, PropertyInfo.DeclaringType));
        }

    }

}
