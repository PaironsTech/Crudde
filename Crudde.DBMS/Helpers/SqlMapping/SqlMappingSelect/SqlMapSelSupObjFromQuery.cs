﻿using System;
using System.Collections.Generic;
using System.Linq;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlMapping
{

    /// <summary>
    /// Support object for mapping objects returned from query
    /// </summary>
    public class SqlMapSelSupObjFromQuery
    {

        /// <summary>
        /// Type of the Model returned from query
        /// </summary>
        public Type TypeModelFromQuery { get; }

        /// <summary>
        /// List of Support Object for Mapping that contain a Property with Foreign Key Mapped Attribute
        /// </summary>
        public List<SqlMapSelSupFKMapped> ListForeignKeyMapped { get; }



        /// <summary>
        /// Constructor of 'SqlMapSelSupObjFromQuery'
        /// </summary>
        /// <param name="typeModelFromQuery"></param>
        public SqlMapSelSupObjFromQuery(Type typeModelFromQuery)
        {
            TypeModelFromQuery = typeModelFromQuery;
            ListForeignKeyMapped = TypeModelFromQuery
                .GetTypeForeignKeyMappedProperties()
                .Select(s => new SqlMapSelSupFKMapped(s))
                .ToList();
        }

    }

}
