﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.SqlBinders;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlMapping
{

    /// <summary>
    /// Static Class with useful methods for mapping Result
    /// </summary>
    internal static class SqlMappingSelect
    {

        /// <summary>
        /// Method that Map Query result
        /// </summary>
        /// <typeparam name="TModelFrom"></typeparam>
        /// <param name="sqlBinderSelect"></param>
        /// <param name="dbConnection"></param>
        /// <param name="sbSql"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <param name="commandType"></param>
        /// <returns></returns>
        public static IEnumerable<TModelFrom> MapQueryResult<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect, IDbConnection dbConnection, 
            StringBuilder sbSql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null,
            CommandType? commandType = null) where TModelFrom : IModelSelectable
        {
            if (sqlBinderSelect.ListJoins.Count == 0) return dbConnection.Query<TModelFrom>(sbSql.ToString(), param, transaction, buffered, commandTimeout, commandType);

            // Execute Control for Auto Mapping of Joins
            ExecuteMappingControl(sqlBinderSelect);

            // Find through Reflection the Mapping Method that generate the function
            Type[] arrGenericTypeForMappingMethod = GetGenericTypeArrayForMappingMethod(sqlBinderSelect);
            MethodInfo mappingMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetMappingFunc", arrGenericTypeForMappingMethod.Length, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            MethodInfo mappingMethod = mappingMethodWithouGenericTypeSet.MakeGenericMethod(arrGenericTypeForMappingMethod);

            // Find through Reflection the Query Method of Dapper
            Type[] arrGenericTypeForQueryMethod = GetGenericTypeArrayForQuery(sqlBinderSelect);
            MethodInfo queryMethodWithoutGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMapper), "Query", arrGenericTypeForQueryMethod.Length, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
            MethodInfo queryMethod = queryMethodWithoutGenericTypeSet.MakeGenericMethod(arrGenericTypeForQueryMethod);

            // Create SplitOn string
            string splitValues = string.Join(",", sqlBinderSelect.ListJoins.Select(s => s.TypeModelToJoin.GetTypeKeyOrExplicitKeyProperty()?.GetPropertyInfoColumnName()));

            // Create List of 'SqlMapSelSupJoin' object that contains all objects to map for every Join
            List<SqlMapSelSupObjFromQuery> listObjectsFromQuery = GetListObjectsFromQuery(sqlBinderSelect);
            
            // Execute Query, Split on Keys, Mapping and return Ienumerable<TModelFrom>
            IEnumerable<TModelFrom> ienumerableToReturn = (IEnumerable<TModelFrom>)queryMethod.Invoke(null, new object[]
            {
                dbConnection,
                sbSql.ToString(),
                mappingMethod.Invoke(null, new object[] { sqlBinderSelect, listObjectsFromQuery }),
                param,
                transaction,
                buffered,
                splitValues,
                commandTimeout,
                commandType
            });

            // If I have Mapping One to Many I use .Distinct() method
            if (ienumerableToReturn.Count() != 0)
            {
                Type[] typesOfIenumerable = ienumerableToReturn.GetType().GetGenericArguments();
                if (typesOfIenumerable.Length != 0)
                {
                    List<PropertyInfo> propsForeignKeyMapped = typesOfIenumerable[0].GetTypeForeignKeyMappedProperties();
                    if (propsForeignKeyMapped.Find(prop => IsListOrArray(prop)) != null)
                    {
                        ienumerableToReturn = ienumerableToReturn
                            .Where(w => w != null)
                            .GroupBy(g => g.GetType().GetTypeKeyOrExplicitKeyProperty().GetValue(g))
                            .Select(s => s.First());
                    }
                }
            }
            
            return ienumerableToReturn;
        }



        //
        // Private Methods
        //


        private static void ExecuteMappingControl<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect) where TModelFrom : IModelSelectable
        {
            // Check max of 6 Joins (Dapper mapping function work only with seven T object where one is the TFrom)
            if (sqlBinderSelect.ListJoins.Count > 6) throw new Exception("Auto Mapping of SqlBuilder work only with max 6 joins!");
        }

        private static Type[] GetGenericTypeArrayForMappingMethod<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect) where TModelFrom : IModelSelectable
        {
            List<Type> types = new List<Type>();
            types.Add(typeof(TModelFrom));
            types.AddRange(sqlBinderSelect.ListJoins.Select(s => s.TypeModelToJoin));
            return types.ToArray();
        }

        private static Type[] GetGenericTypeArrayForQuery<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect) where TModelFrom : IModelSelectable
        {
            List<Type> types = new List<Type>();
            types.Add(typeof(TModelFrom));
            types.AddRange(sqlBinderSelect.ListJoins.Select(s => s.TypeModelToJoin));
            types.Add(typeof(TModelFrom));
            return types.ToArray();
        }

        private static MethodInfo GetMethodWithOverloadAndGenericType(Type typeStaticClass, string methodName, int numberGenericType, BindingFlags bindingFlags)
        {
            MethodInfo staticMethod = typeStaticClass.GetMethods(bindingFlags).ToList().Find(f => f.IsGenericMethodDefinition && f.GetGenericArguments().Length == numberGenericType && f.Name == methodName);
            if (staticMethod == null) throw new Exception("Method not finded!");
            return staticMethod;
        }

        private static bool IsListOrArray(PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable));
        }

        private static List<SqlMapSelSupObjFromQuery> GetListObjectsFromQuery<TModelFrom>(SqlBinderSelect<TModelFrom> sqlBinderSelect) where TModelFrom : IModelSelectable
        {
            List<SqlMapSelSupObjFromQuery> retList = new List<SqlMapSelSupObjFromQuery>();

            retList.Add(new SqlMapSelSupObjFromQuery(typeof(TModelFrom)));

            sqlBinderSelect.ListJoins.ForEach(f =>
            {
                retList.Add(new SqlMapSelSupObjFromQuery(f.TypeModelToJoin));
            });

            return retList;
        }

        private static TModelFromType GetObjectFromType<TModelFromType>(Type typeObjectToSearch, IModelSelectable[] modelsToMapping)
                where TModelFromType : IModelSelectable
        {
            return (TModelFromType)modelsToMapping.ToList().Find(f => f != null && f.GetType() == typeObjectToSearch);
        }

        private static void MapObject<TModelToBeValued, TModelToBeAssigned, TPrimaryKeyModelToBeValued>
            (TModelToBeValued modelToBeValued, PropertyInfo propertyToBeValued, TModelToBeAssigned modelToBeAssigned, Dictionary<TPrimaryKeyModelToBeValued, TModelToBeValued> dictionaryMapOneToMany)
            where TModelToBeValued : IModelSelectable where TModelToBeAssigned : IModelSelectable
        {
            if (!IsListOrArray(propertyToBeValued))
            {
                // Execute Mapping One To One
                propertyToBeValued.SetValue(modelToBeValued, modelToBeAssigned);
            }
            else
            {
                TModelToBeValued modelToBeValuedEntry = default(TModelToBeValued);
                PropertyInfo keyProperty = modelToBeValued.GetType().GetTypeKeyOrExplicitKeyProperty();
                TPrimaryKeyModelToBeValued keyValue = (TPrimaryKeyModelToBeValued)keyProperty.GetValue(modelToBeValued);

                if (keyProperty != null && !dictionaryMapOneToMany.TryGetValue(keyValue, out modelToBeValuedEntry))
                {
                    modelToBeValuedEntry = modelToBeValued;
                    propertyToBeValued.SetValue(modelToBeValuedEntry, Activator.CreateInstance(propertyToBeValued.PropertyType));    // Create new instance of IEnumerable<T>
                    dictionaryMapOneToMany.Add(keyValue, modelToBeValuedEntry);
                }

                if (modelToBeAssigned != null)
                {
                    // Adding Element at list
                    propertyToBeValued.PropertyType.GetMethod("Add").Invoke(propertyToBeValued.GetValue(modelToBeValuedEntry), new IModelSelectable[] { modelToBeAssigned });

                    // Add to property value new list values
                    propertyToBeValued.SetValue(modelToBeValued, propertyToBeValued.GetValue(modelToBeValuedEntry));
                }
            }
        }

        private static bool CheckIfObjIsDefault(object obj)
        {
            bool isDefault = true;

            if (obj != null)
            {
                if (obj is ValueType)
                {
                    if (!obj.Equals(Activator.CreateInstance(obj.GetType())))
                        isDefault = false;
                }
                else if (obj is string)
                {
                    if (string.IsNullOrEmpty((string)obj))
                        isDefault = false;
                }
                else
                {
                    obj.GetType().GetProperties().ToList().ForEach(f =>
                    {
                        if (!CheckIfObjIsDefault(f.GetValue(obj)))
                            isDefault = false;
                    });
                }
            }
            
            return isDefault;
        }

        private static void CheckAndMapObjects(MethodInfo getObjectFromTypeMethodWithouGenericTypeSet, MethodInfo mapJoinMethodWithouGenericTypeSet, List<SqlMapSelSupObjFromQuery> listJoinObjectsToMap, IModelSelectable[] modelSelectables)
        {
            // Cycle List of object that can be mapped 
            listJoinObjectsToMap.ForEach(f =>
            {
                var objectToBeValued = getObjectFromTypeMethodWithouGenericTypeSet
                    .MakeGenericMethod(f.TypeModelFromQuery)
                    .Invoke(null, new object[] { f.TypeModelFromQuery, modelSelectables });

                if (objectToBeValued != null)
                {
                    // Cycle the foreign keys of object to be valued and find if in IModelSelectable array there is an object with the type of the foreign key.
                    f.ListForeignKeyMapped.ForEach(fe =>
                    {
                        // Find the object to be assigned with the type of the property
                        var objectToBeAssigned = getObjectFromTypeMethodWithouGenericTypeSet
                                .MakeGenericMethod(fe.TypeProperty)
                                .Invoke(null, new object[] { fe.TypeProperty, modelSelectables });

                        if (objectToBeAssigned != null)
                        {
                            // Call 'MapJoin' method
                            mapJoinMethodWithouGenericTypeSet
                                .MakeGenericMethod(objectToBeValued.GetType(), objectToBeAssigned.GetType(), fe.TypePrimaryKey)
                                .Invoke(null, new object[]
                                {
                                    objectToBeValued,
                                    fe.PropertyInfo,
                                    objectToBeAssigned,
                                    fe.DictionaryOneToMany
                                });
                        }
                    });
                }

            });
        }




        //
        // Mapping Func with different sign
        //

        private static Func<TModelFrom, TModelJoinOne, TModelFrom> GetMappingFunc<TModelFrom, TModelJoinOne>(SqlBinderSelect<TModelFrom> sqlBinderSelect, List<SqlMapSelSupObjFromQuery> listObjectsFromQuery) 
            where TModelFrom : IModelSelectable where TModelJoinOne : IModelSelectable
        {
            // Find through Reflection the Method that get the correctObject from paramters of function
            MethodInfo getObjectFromTypeMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetObjectFromType", 1, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Find through Reflection the Method of Mapping
            MethodInfo mapJoinMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "MapObject", 3, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Return the Function of Mapping
            return (TModelFrom from, TModelJoinOne joinOne) =>
            {
                if (CheckIfObjIsDefault(from)) return default(TModelFrom);

                IModelSelectable[] modelSelectables = new IModelSelectable[] { from, joinOne };

                CheckAndMapObjects(getObjectFromTypeMethodWithouGenericTypeSet, mapJoinMethodWithouGenericTypeSet, listObjectsFromQuery, modelSelectables);

                return from;
            };
        }

        private static Func<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelFrom> GetMappingFunc<TModelFrom, TModelJoinOne, TModelJoinTwo>(SqlBinderSelect<TModelFrom> sqlBinderSelect, List<SqlMapSelSupObjFromQuery> listObjectsFromQuery)
            where TModelFrom : IModelSelectable where TModelJoinOne : IModelSelectable where TModelJoinTwo : IModelSelectable
        {

            // Find through Reflection the Method that get the correctObject from paramters of function
            MethodInfo getObjectFromTypeMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetObjectFromType", 1, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Find through Reflection the Method of Mapping
            MethodInfo mapJoinMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "MapObject", 3, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Return the Function of Mapping
            return (TModelFrom from, TModelJoinOne joinOne, TModelJoinTwo joinTwo) =>
            {
                if (CheckIfObjIsDefault(from)) return default(TModelFrom);

                IModelSelectable[] modelSelectables = new IModelSelectable[] { from, joinOne, joinTwo };

                CheckAndMapObjects(getObjectFromTypeMethodWithouGenericTypeSet, mapJoinMethodWithouGenericTypeSet, listObjectsFromQuery, modelSelectables);

                return from;
            };
        }

        private static Func<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelJoinThree, TModelFrom> GetMappingFunc<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelJoinThree>(
            SqlBinderSelect<TModelFrom> sqlBinderSelect, List<SqlMapSelSupObjFromQuery> listObjectsFromQuery)
            where TModelFrom : IModelSelectable where TModelJoinOne : IModelSelectable where TModelJoinTwo : IModelSelectable where TModelJoinThree : IModelSelectable
        {

            // Find through Reflection the Method that get the correctObject from paramters of function
            MethodInfo getObjectFromTypeMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetObjectFromType", 1, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Find through Reflection the Method of Mapping
            MethodInfo mapJoinMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "MapObject", 3, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Return the Function of Mapping
            return (TModelFrom from, TModelJoinOne joinOne, TModelJoinTwo joinTwo, TModelJoinThree joinThree) =>
            {
                if (CheckIfObjIsDefault(from)) return default(TModelFrom);

                IModelSelectable[] modelSelectables = new IModelSelectable[] { from, joinOne, joinTwo, joinThree };

                CheckAndMapObjects(getObjectFromTypeMethodWithouGenericTypeSet, mapJoinMethodWithouGenericTypeSet, listObjectsFromQuery, modelSelectables);

                return from;
            };
        }

        private static Func<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelJoinThree, TModelJoinFour, TModelFrom> GetMappingFunc<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelJoinThree,
            TModelJoinFour>(SqlBinderSelect<TModelFrom> sqlBinderSelect, List<SqlMapSelSupObjFromQuery> listObjectsFromQuery)
            where TModelFrom : IModelSelectable where TModelJoinOne : IModelSelectable where TModelJoinTwo : IModelSelectable where TModelJoinThree : IModelSelectable 
            where TModelJoinFour : IModelSelectable
        {

            // Find through Reflection the Method that get the correctObject from paramters of function
            MethodInfo getObjectFromTypeMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetObjectFromType", 1, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Find through Reflection the Method of Mapping
            MethodInfo mapJoinMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "MapObject", 3, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Return the Function of Mapping
            return (TModelFrom from, TModelJoinOne joinOne, TModelJoinTwo joinTwo, TModelJoinThree joinThree, TModelJoinFour joinFour) =>
            {
                if (CheckIfObjIsDefault(from)) return default(TModelFrom);

                IModelSelectable[] modelSelectables = new IModelSelectable[] { from, joinOne, joinTwo, joinThree, joinFour };

                CheckAndMapObjects(getObjectFromTypeMethodWithouGenericTypeSet, mapJoinMethodWithouGenericTypeSet, listObjectsFromQuery, modelSelectables);

                return from;
            };
        }

        private static Func<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelJoinThree, TModelJoinFour, TModelJoinFive, TModelFrom> GetMappingFunc<TModelFrom, TModelJoinOne, TModelJoinTwo, 
            TModelJoinThree, TModelJoinFour, TModelJoinFive>(SqlBinderSelect<TModelFrom> sqlBinderSelect, List<SqlMapSelSupObjFromQuery> listObjectsFromQuery)
            where TModelFrom : IModelSelectable where TModelJoinOne : IModelSelectable where TModelJoinTwo : IModelSelectable where TModelJoinThree : IModelSelectable 
            where TModelJoinFour : IModelSelectable where TModelJoinFive : IModelSelectable
        {

            // Find through Reflection the Method that get the correctObject from paramters of function
            MethodInfo getObjectFromTypeMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetObjectFromType", 1, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Find through Reflection the Method of Mapping
            MethodInfo mapJoinMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "MapObject", 3, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Return the Function of Mapping
            return (TModelFrom from, TModelJoinOne joinOne, TModelJoinTwo joinTwo, TModelJoinThree joinThree, TModelJoinFour joinFour, TModelJoinFive joinFive) =>
            {
                if (CheckIfObjIsDefault(from)) return default(TModelFrom);

                IModelSelectable[] modelSelectables = new IModelSelectable[] { from, joinOne, joinTwo, joinThree, joinFour, joinFive };

                CheckAndMapObjects(getObjectFromTypeMethodWithouGenericTypeSet, mapJoinMethodWithouGenericTypeSet, listObjectsFromQuery, modelSelectables);

                return from;
            };
        }

        private static Func<TModelFrom, TModelJoinOne, TModelJoinTwo, TModelJoinThree, TModelJoinFour, TModelJoinFive, TModelJoinSix, TModelFrom> GetMappingFunc<TModelFrom, TModelJoinOne, 
            TModelJoinTwo, TModelJoinThree, TModelJoinFour, TModelJoinFive, TModelJoinSix>(SqlBinderSelect<TModelFrom> sqlBinderSelect, List<SqlMapSelSupObjFromQuery> listObjectsFromQuery)
            where TModelFrom : IModelSelectable where TModelJoinOne : IModelSelectable where TModelJoinTwo : IModelSelectable where TModelJoinThree : IModelSelectable 
            where TModelJoinFour : IModelSelectable where TModelJoinFive : IModelSelectable where TModelJoinSix : IModelSelectable
        {

            // Find through Reflection the Method that get the correctObject from paramters of function
            MethodInfo getObjectFromTypeMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "GetObjectFromType", 1, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Find through Reflection the Method of Mapping
            MethodInfo mapJoinMethodWithouGenericTypeSet = GetMethodWithOverloadAndGenericType(typeof(SqlMappingSelect), "MapObject", 3, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);

            // Return the Function of Mapping
            return (TModelFrom from, TModelJoinOne joinOne, TModelJoinTwo joinTwo, TModelJoinThree joinThree, TModelJoinFour joinFour, TModelJoinFive joinFive, TModelJoinSix joinSix) =>
            {
                if (CheckIfObjIsDefault(from)) return default(TModelFrom);

                IModelSelectable[] modelSelectables = new IModelSelectable[] { from, joinOne, joinTwo, joinThree, joinFour, joinFive, joinSix };

                CheckAndMapObjects(getObjectFromTypeMethodWithouGenericTypeSet, mapJoinMethodWithouGenericTypeSet, listObjectsFromQuery, modelSelectables);

                return from;
            };
        }

    }
}
