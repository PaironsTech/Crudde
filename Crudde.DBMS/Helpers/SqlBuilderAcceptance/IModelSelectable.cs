﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance
{

    /// <summary>
    /// Interface for Model that can be use "Select" command
    /// </summary>
    public interface IModelSelectable { }

}
