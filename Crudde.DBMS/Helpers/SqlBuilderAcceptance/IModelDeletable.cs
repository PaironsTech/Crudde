﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance
{

    /// <summary>
    /// Interface for Model that can be use "Delete" command
    /// </summary>
    public interface IModelDeletable { }

}
