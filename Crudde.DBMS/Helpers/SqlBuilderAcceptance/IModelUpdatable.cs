﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance
{

    /// <summary>
    /// Interface for Model that can be use "Update" command
    /// </summary>
    public interface IModelUpdatable { }

}
