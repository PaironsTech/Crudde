﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance
{

    /// <summary>
    /// Interface for Model that can be use "Drop" command
    /// </summary>
    public interface IModelDroppable { }

}
