﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance
{

    /// <summary>
    /// Interface for Model that can be use "Insert" command
    /// </summary>
    public interface IModelInsertable { }

}
