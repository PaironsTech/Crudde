﻿namespace PaironsTech.Crudde.DBMS
{

    /// <summary>
    /// Public static class that contain extension methods for "long" object
    /// </summary>
    public static class PubExtensionMethodsLong
    {

        /// <summary>
        /// Extension Method for SqlBuilders
        /// </summary>
        /// <param name="field"></param>
        /// <param name="arrInValues"></param>
        /// <returns></returns>
        public static bool In(this long field, long[] arrInValues)
        {
            return true;
        }

    }

}
