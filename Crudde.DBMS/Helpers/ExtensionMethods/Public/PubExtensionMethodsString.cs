﻿namespace PaironsTech.Crudde.DBMS
{

    /// <summary>
    /// Public static class that contain extension methods for "string" object
    /// </summary>
    public static class PubExtensionMethodsString
    {

        /// <summary>
        /// Extension Method for SqlBuilders
        /// </summary>
        /// <param name="field"></param>
        /// <param name="likeStr"></param>
        public static bool Like(this string field, string likeStr)
        {
            return true;
        }

        /// <summary>
        /// Extension Method for SqlBuilders
        /// </summary>
        /// <param name="field"></param>
        /// <param name="likeStr"></param>
        public static bool ILike(this string field, string likeStr)
        {
            return true;
        }

        /// <summary>
        /// Extension Method for SqlBuilders
        /// </summary>
        /// <param name="field"></param>
        /// <param name="arrInValues"></param>
        /// <returns></returns>
        public static bool In(this string field, string[] arrInValues)
        {
            return true;
        }

    }
}
