﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.Helpers.SqlParameters;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "Expression" object
    /// </summary>
    internal static class IntExtensionMethodsExpression
    {

        /// <summary>
        /// Extract from an Expression a list of PropertyInfoExtended
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetPropertiesFromExpression(this Expression expression)
        {
            List<PropertyInfo> props = new List<PropertyInfo>();

            if (expression is NewArrayExpression arrayExpressions)
            {
                foreach (Expression itemExpression in arrayExpressions.Expressions)
                    props.Add(GetPropertyFromExpression(itemExpression));
            }
            else
                props.Add(GetPropertyFromExpression(expression));

            return props;
        }


        /// <summary>
        /// Extract the single PropertyInfoExtended from an Expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal static PropertyInfo GetPropertyFromExpression(this Expression expression)
        {
            if (expression is MemberExpression memberExpression) return (PropertyInfo)memberExpression.Member;
            if (expression is UnaryExpression unaryExpression) return GetPropertyFromExpression(unaryExpression.Operand);

            throw new Exception("This type of expression is not implemented yet!");
        }


        /// <summary>
        /// Get the condition object from an Expression
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        internal static Condition GetCondition(this Expression expression, params object[] objects)
        {
            Condition condition = null;

            if (expression is BinaryExpression binaryExpression)
            {
                condition = new Condition()
                {
                    ConditionLeftNode = binaryExpression.Left.GetCondition(),
                    ConditionOperand = binaryExpression.NodeType.GetOperand(),
                    ConditionRightNode = binaryExpression.Right.GetCondition()
                };
            }


            if (expression is BlockExpression blockExpression)
            {
                throw new NotImplementedException("The expression type 'BlockExpression' is not supported yet!");
            }


            if (expression is ConditionalExpression conditionalExpression)
            {
                throw new NotImplementedException("The expression type 'ConditionalExpression' is not supported yet!");
            }


            if (expression is ConstantExpression constantExpression)
            {
                condition = new Condition()
                {
                    IsNode = true,
                    NodeConditionType = TypeNodeCondition.Constant,
                    NodeConditionValue = constantExpression.GetValueFromExpression()
                };
            }


            if (expression is DebugInfoExpression debugInfoExpression)
            {
                throw new NotImplementedException("The expression type 'DebugInfoExpression' is not supported yet!");
            }


            if (expression is DefaultExpression defaultExpression)
            {
                throw new NotImplementedException("The expression type 'DefaultExpression' is not supported yet!");
            }


            if (expression is DynamicExpression dynamicExpression)
            {
                throw new NotImplementedException("The expression type 'DynamicExpression' is not supported yet!");
            }


            if (expression is GotoExpression gotoExpression)
            {
                throw new NotImplementedException("The expression type 'GotoExpression' is not supported yet!");
            }


            if (expression is IndexExpression indexExpression)
            {
                throw new NotImplementedException("The expression type 'IndexExpression' is not supported yet!");
            }


            if (expression is InvocationExpression invocationExpression)
            {
                throw new NotImplementedException("The expression type 'InvocationExpression' is not supported yet!");
            }


            if (expression is LabelExpression labelExpression)
            {
                throw new NotImplementedException("The expression type 'LabelExpression' is not supported yet!");
            }


            if (expression is LambdaExpression lambdaExpression)
            {
                throw new NotImplementedException("The expression type 'LambdaExpression' is not supported yet!");
            }


            if (expression is ListInitExpression listInitExpression)
            {
                throw new NotImplementedException("The expression type 'ListInitExpression' is not supported yet!");
            }


            if (expression is LoopExpression loopExpression)
            {
                throw new NotImplementedException("The expression type 'LoopExpression' is not supported yet!");
            }


            if (expression is MemberExpression memberExpression)
            {
                if (memberExpression.Member != null)
                {
                    switch (memberExpression.Member.MemberType)
                    {
                        case MemberTypes.Constructor:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Contructor' is not implemented yet!");

                        case MemberTypes.Custom:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Custom' is not implemented yet!");

                        case MemberTypes.Event:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Event' is not implemented yet!");

                        case MemberTypes.Field:
                            condition = new Condition()
                            {
                                IsNode = true,
                                NodeConditionType = TypeNodeCondition.Constant,
                                NodeConditionValue = memberExpression.GetValueFromExpression()
                            };
                            break;

                        case MemberTypes.Method:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Method' is not implemented yet!");

                        case MemberTypes.NestedType:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'NestedType' is not implemented yet!");

                        case MemberTypes.Property:

                            if (memberExpression.Expression.NodeType == ExpressionType.Parameter)
                            {
                                condition = new Condition()
                                {
                                    IsNode = true,
                                    NodeConditionType = TypeNodeCondition.Property,
                                    NodeConditionProperty = (PropertyInfo)memberExpression.Member
                                };
                            }
                            else if (memberExpression.Expression.Type == typeof(ParamValue))
                            {
                                condition = new Condition()
                                {
                                    IsNode = true,
                                    NodeConditionType = TypeNodeCondition.ParamValue,
                                    NameParamValue = ((ParamValue)memberExpression.Expression.GetValueFromExpression()).NameParamValue
                                };
                            }
                            else
                            {
                                condition = new Condition()
                                {
                                    IsNode = true,
                                    NodeConditionType = TypeNodeCondition.Constant,
                                    NodeConditionValue = memberExpression.GetValueFromExpression()
                                };
                            }

                            break;

                        case MemberTypes.TypeInfo:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'TypeInfo' is not implemented yet!");

                    }
                }
                else
                {
                    throw new NotImplementedException("The expression type 'MemberExpression' have the property 'Member' equals to NULL!");
                }
            }


            if (expression is MemberInitExpression memberInitExpression)
            {
                throw new NotImplementedException("The expression type 'MemberInitExpression' is not supported yet!");
            }


            if (expression is MethodCallExpression methodCallExpression)
            {
                if (methodCallExpression.Method != null)
                {
                    switch (methodCallExpression.Method.Name)
                    {

                        case "Like":
                            condition = new Condition()
                            {
                                ConditionLeftNode = methodCallExpression.Arguments[0].GetCondition(),
                                ConditionOperand = TypeOperandCondition.Like,
                                ConditionRightNode = methodCallExpression.Arguments[1].GetCondition()
                            };
                            break;

                        case "ILike":
                            condition = new Condition()
                            {
                                ConditionLeftNode = methodCallExpression.Arguments[0].GetCondition(),
                                ConditionOperand = TypeOperandCondition.ILike,
                                ConditionRightNode = methodCallExpression.Arguments[1].GetCondition()
                            };
                            break;

                        case "In":
                            condition = new Condition()
                            {
                                ConditionLeftNode = methodCallExpression.Arguments[0].GetCondition(),
                                ConditionOperand = TypeOperandCondition.In,
                                ConditionRightNode = methodCallExpression.Arguments[1].GetCondition()
                            };
                            break;

                        case "get_Item":    // For Dictionary
                            condition = new Condition()
                            {
                                IsNode = true,
                                NodeConditionType = TypeNodeCondition.Constant,
                                NodeConditionValue = Expression.Lambda(methodCallExpression).Compile().DynamicInvoke()
                            };
                            break;

                        default:

                            switch(methodCallExpression.Method.MemberType)
                            {
                                case MemberTypes.Constructor:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'Constructor' yet!");

                                case MemberTypes.Custom:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'Custom' yet!");

                                case MemberTypes.Event:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'Event' yet!");

                                case MemberTypes.Field:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'Field' yet!");

                                case MemberTypes.Method:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'Method' yet!");

                                case MemberTypes.NestedType:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'NestedType' yet!");

                                case MemberTypes.Property:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'Property' yet!");

                                case MemberTypes.TypeInfo:
                                    throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method with MemberTypes equals to 'TypeInfo' yet!");

                            }

                            throw new NotImplementedException("The expression type 'MemberInitExpression' not support the method called '" + methodCallExpression.Method.Name + "' yet!");
                    }
                }
                else
                {
                    throw new NotImplementedException("The expression type 'MemberInitExpression' has the property 'Method' equals to NULL!");
                }
            }


            if (expression is NewArrayExpression newArrayExpression)
            {
                throw new NotImplementedException("The expression type 'NewArrayExpression' is not supported yet!");
            }


            if (expression is ParameterExpression parameterExpression)
            {
                throw new NotImplementedException("The expression type 'ParameterExpression' is not supported yet!");
            }


            if (expression is RuntimeVariablesExpression runtimeVariablesExpression)
            {
                throw new NotImplementedException("The expression type 'RuntimeVariablesExpression' is not supported yet!");
            }


            if (expression is SwitchExpression switchExpression)
            {
                throw new NotImplementedException("The expression type 'SwitchExpression' is not supported yet!");
            }


            if (expression is TryExpression tryExpression)
            {
                throw new NotImplementedException("The expression type 'TryExpression' is not supported yet!");
            }


            if (expression is TypeBinaryExpression typeBinaryExpression)
            {
                throw new NotImplementedException("The expression type 'TypeBinaryExpression' is not supported yet!");
            }


            if (expression is UnaryExpression unaryExpression)
            {
                throw new NotImplementedException("The expression type 'UnaryExpression' is not supported yet!");
            }

            return condition;
        }


        /// <summary>
        /// Get the left node of an expression condition
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal static Expression GetLeftNode(this Expression expression)
        {
            dynamic exp = expression;
            return ((Expression)exp.Left);
        }


        /// <summary>
        /// Get the right node of an expression condition
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal static Expression GetRightNode(this Expression expression)
        {
            dynamic exp = expression;
            return ((Expression)exp.Right);
        }


        /// <summary>
        /// Get the 'Final' value of an expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        internal static object GetValueFromExpression(this Expression expression)
        {
            object ret = null;

            if (expression is BinaryExpression binaryExpression)
            {
                throw new NotImplementedException("The expression type 'BinaryExpression' is not supported yet!");
            }


            if (expression is BlockExpression blockExpression)
            {
                throw new NotImplementedException("The expression type 'BlockExpression' is not supported yet!");
            }


            if (expression is ConditionalExpression conditionalExpression)
            {
                throw new NotImplementedException("The expression type 'ConditionalExpression' is not supported yet!");
            }


            if (expression is ConstantExpression constantExpression)
            {
                ret = constantExpression.Value;
                goto end;
            }


            if (expression is DebugInfoExpression debugInfoExpression)
            {
                throw new NotImplementedException("The expression type 'DebugInfoExpression' is not supported yet!");
            }


            if (expression is DefaultExpression defaultExpression)
            {
                throw new NotImplementedException("The expression type 'DefaultExpression' is not supported yet!");
            }


            if (expression is DynamicExpression dynamicExpression)
            {
                throw new NotImplementedException("The expression type 'DynamicExpression' is not supported yet!");
            }


            if (expression is GotoExpression gotoExpression)
            {
                throw new NotImplementedException("The expression type 'GotoExpression' is not supported yet!");
            }


            if (expression is IndexExpression indexExpression)
            {
                throw new NotImplementedException("The expression type 'IndexExpression' is not supported yet!");
            }


            if (expression is InvocationExpression invocationExpression)
            {
                throw new NotImplementedException("The expression type 'InvocationExpression' is not supported yet!");
            }


            if (expression is LabelExpression labelExpression)
            {
                throw new NotImplementedException("The expression type 'LabelExpression' is not supported yet!");
            }


            if (expression is LambdaExpression lambdaExpression)
            {
                throw new NotImplementedException("The expression type 'LambdaExpression' is not supported yet!");
            }


            if (expression is ListInitExpression listInitExpression)
            {
                throw new NotImplementedException("The expression type 'ListInitExpression' is not supported yet!");
            }


            if (expression is LoopExpression loopExpression)
            {
                throw new NotImplementedException("The expression type 'LoopExpression' is not supported yet!");
            }


            if (expression is MemberExpression memberExpression)
            {
                if (memberExpression.Member != null)
                {
                    switch (memberExpression.Member.MemberType)
                    {
                        case MemberTypes.Constructor:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Contructor' is not implemented yet!");

                        case MemberTypes.Custom:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Custom' is not implemented yet!");

                        case MemberTypes.Event:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Event' is not implemented yet!");

                        case MemberTypes.Field:
                            var objectMemberField = Expression.Convert(memberExpression, typeof(object));
                            var getterLambdaField = Expression.Lambda<Func<object>>(objectMemberField);
                            var getterField = getterLambdaField.Compile();
                            ret = getterField();
                            goto end;

                        case MemberTypes.Method:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'Method' is not implemented yet!");

                        case MemberTypes.NestedType:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'NestedType' is not implemented yet!");

                        case MemberTypes.Property:
                            var objectMemberProperty = Expression.Convert(memberExpression, typeof(object));
                            var getterLambdaProperty = Expression.Lambda<Func<object>>(objectMemberProperty);
                            var getterProperty = getterLambdaProperty.Compile();
                            ret = getterProperty();
                            goto end;

                        case MemberTypes.TypeInfo:
                            throw new NotImplementedException("The expression type 'MemberExpression' with property 'MemberType' equals to 'TypeInfo' is not implemented yet!");

                    }
                }
                else
                {
                    throw new NotImplementedException("The expression type 'MemberExpression' have the property 'Member' equals to NULL!");
                }
            }


            if (expression is MemberInitExpression memberInitExpression)
            {
                throw new NotImplementedException("The expression type 'MemberInitExpression' is not supported yet!");
            }


            if (expression is MethodCallExpression methodCallExpression)
            {
                throw new NotImplementedException("The expression type 'MethodCallExpression' is not supported yet!");
            }


            if (expression is NewArrayExpression newArrayExpression)
            {
                throw new NotImplementedException("The expression type 'NewArrayExpression' is not supported yet!");
            }


            if (expression is ParameterExpression parameterExpression)
            {
                throw new NotImplementedException("The expression type 'ParameterExpression' is not supported yet!");
            }


            if (expression is RuntimeVariablesExpression runtimeVariablesExpression)
            {
                throw new NotImplementedException("The expression type 'RuntimeVariablesExpression' is not supported yet!");
            }


            if (expression is SwitchExpression switchExpression)
            {
                throw new NotImplementedException("The expression type 'SwitchExpression' is not supported yet!");
            }


            if (expression is TryExpression tryExpression)
            {
                throw new NotImplementedException("The expression type 'TryExpression' is not supported yet!");
            }


            if (expression is TypeBinaryExpression typeBinaryExpression)
            {
                throw new NotImplementedException("The expression type 'TypeBinaryExpression' is not supported yet!");
            }


            if (expression is UnaryExpression unaryExpression)
            {
                throw new NotImplementedException("The expression type 'UnaryExpression' is not supported yet!");
            }

            end:

            return ret;
        }

    }
}
