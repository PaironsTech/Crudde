﻿using System.Collections.Generic;
using System.Dynamic;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "Dictionary" object
    /// </summary>
    internal static class IntExtensionMethodsDictionary
    {

        /// <summary>
        /// Extension Method for convert dictionary to object that have the properties with the name of the key
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        internal static object ConvertToObject(this Dictionary<string, object> dictionary)
        {
            ExpandoObject eo = new ExpandoObject();
            ICollection<KeyValuePair<string, object>> eoColl = eo;

            foreach (KeyValuePair<string, object> kvp in dictionary)
                eoColl.Add(kvp);

            return eo;
        }

    }

}
