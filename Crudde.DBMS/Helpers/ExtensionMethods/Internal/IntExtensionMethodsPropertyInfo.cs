﻿using System.Reflection;
using PaironsTech.Crudde.DBMS.Attributes;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "PropertyInfo" object
    /// </summary>
    internal static class IntExtensionMethodsPropertyInfo
    {

        /// <summary>
        /// Return correct column name of property info
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        internal static string GetPropertyInfoColumnName(this PropertyInfo propertyInfo)
        {
            var attributes = propertyInfo.GetCustomAttributes(typeof(ColumnAttribute), false).AsList();
            if (attributes.Count != 0) return ((ColumnAttribute)attributes[0]).Name;
            return propertyInfo.Name;
        }


        /// <summary>
        /// Return a boolean that check if the property is Writeable (without NotWrite Attribute)
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        internal static bool GetPropertyInfoIsWriteable(this PropertyInfo propertyInfo)
        {
            var attributes = propertyInfo.GetCustomAttributes(typeof(NotWriteAttribute), false).AsList();
            if (attributes.Count != 0) return true;
            return false;
        }


        /// <summary>
        /// Return the correct name in db of the model associated at this property info
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        internal static string GetPropertyInfoTableName(this PropertyInfo propertyInfo)
        {
            return propertyInfo.DeclaringType.GetTrueModelName();
        }

    }
}
