﻿using System.Collections.Generic;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;
using PaironsTech.Crudde.DBMS.SqlAdapters;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "Condition" object
    /// </summary>
    internal static class IntExtensionMethodsCondition
    {

        /// <summary>
        /// Bind the Condition object and translate it in Sql based on the correct SqlAdapter
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="sqlAdapter"></param>
        /// <param name="queryParams"></param>
        /// <param name="queryParamsUpdated"></param>
        /// <param name="prefixConstantValue"></param>
        /// <param name="postfixConstantValue"></param>
        /// <returns></returns>
        internal static string Bind(this Condition condition, ISqlAdapter sqlAdapter, Dictionary<string, object> queryParams, out Dictionary<string, object> queryParamsUpdated, string prefixConstantValue = null, string postfixConstantValue = null)
        {
            string conditionBinded = string.Empty;
            queryParamsUpdated = queryParams;

            if (condition.IsNode)
            {
                switch (condition.NodeConditionType)
                {
                    case TypeNodeCondition.Constant:
                        conditionBinded = prefixConstantValue + "@" + SqlParameters.SqlParameter.GetNameParam(queryParams, condition.NodeConditionValue, out queryParamsUpdated) + postfixConstantValue;
                        break;

                    case TypeNodeCondition.Property:
                        conditionBinded = sqlAdapter.BindTableName(condition.NodeConditionProperty.GetPropertyInfoTableName()) + "." + sqlAdapter.BindColumnName(condition.NodeConditionProperty.GetPropertyInfoColumnName());
                        break;

                    case TypeNodeCondition.ParamValue:
                        conditionBinded = prefixConstantValue + "@" + condition.NameParamValue + postfixConstantValue;
                        break;
                }
            }
            else
            {
                // I get immediatly the operand for get the possible prefix and postfix for constant value
                string operandValue = sqlAdapter.BindOperandCondition((TypeOperandCondition)condition.ConditionOperand, out prefixConstantValue, out postfixConstantValue);

                conditionBinded = "(" +
                    condition.ConditionLeftNode.Bind(sqlAdapter, queryParams, out queryParamsUpdated, prefixConstantValue, postfixConstantValue) + " " +
                    operandValue + " " +
                    condition.ConditionRightNode.Bind(sqlAdapter, queryParams, out queryParamsUpdated, prefixConstantValue, postfixConstantValue) + ")";
            }

            return conditionBinded;
        }

    }
}
