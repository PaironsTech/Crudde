﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Attributes;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "Type" object
    /// </summary>
    internal static class IntExtensionMethodsType
    {

        /// <summary>
        /// Return the true model name. (Table, View, etc. )
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static string GetTrueModelName(this Type type)
        {
            if (Cache.TypeTableAndViewName.TryGetValue(type.TypeHandle, out string name)) return name;

            // NOTE: This as dynamic trick should be able to handle both our own Table-attribute as well as the one in EntityFramework 

            // Table Attribute
            if (typeof(ICruddeTable).IsAssignableFrom(type) || typeof(ICruddeCustomSelect).IsAssignableFrom(type))
            {
                var tableAttr = type
                #if NETSTANDARD1_3
                    .GetTypeInfo()
                #endif
                    .GetCustomAttributes(false).ToList().Find(attr => attr.GetType() == typeof(TableAttribute));

                if (tableAttr != null)
                {
                    name = ((TableAttribute)tableAttr).Name;
                    Cache.TypeTableAndViewName[type.TypeHandle] = name;
                    return name;
                }
            }


            // View Attribute
            if (typeof(ICruddeView).IsAssignableFrom(type) || typeof(ICruddeCustomSelect).IsAssignableFrom(type))
            {
                var viewNameAttr = type
                #if NETSTANDARD1_3
                    .GetTypeInfo()
                #endif
                    .GetCustomAttributes(false).ToList().Find(attr => attr.GetType() == typeof(ViewAttribute));

                if (viewNameAttr != null)
                {
                    name = ((ViewAttribute)viewNameAttr).Name;
                    Cache.TypeTableAndViewName[type.TypeHandle] = name;
                    return name;
                }
            }


            return name;
        }


        /// <summary>
        /// Get the list of PropertyInfo cached for this Type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeProperties(this Type type)
        {
            if (Cache.TypeProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var properties = type.GetProperties().ToList();
            Cache.TypeProperties[type.TypeHandle] = properties;
            return properties;
        }


        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties with Explicit Key Attribute
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeExplicitKeyProperties(this Type type)
        {
            if (Cache.TypeExplicitKeyProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var explicitKeyProperties = type.GetTypeProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is ExplicitKeyAttribute)).ToList();
            Cache.TypeExplicitKeyProperties[type.TypeHandle] = explicitKeyProperties;
            return explicitKeyProperties;
        }


        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties with Foreign Key Mapped Attribute
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeForeignKeyMappedProperties(this Type type)
        {
            if (Cache.TypeForeignKeyMappedProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var foreignKeyMappedProperties = type.GetTypeProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is ForeignKeyObjectAttribute)).ToList();
            Cache.TypeForeignKeyMappedProperties[type.TypeHandle] = foreignKeyMappedProperties;
            return foreignKeyMappedProperties;
        }


        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties with Ignore In Insert Attribute
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeIgnoreInInsertProperties(this Type type)
        {
            if (Cache.TypeIgnoreInInsertProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var ignoreInInsertProperties = type.GetTypeProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is IgnoreInInsertAttribute)).ToList();
            Cache.TypeIgnoreInInsertProperties[type.TypeHandle] = ignoreInInsertProperties;
            return ignoreInInsertProperties;
        }


        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties with Ignore In Update Attribute
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeIgnoreInUpdateProperties(this Type type)
        {
            if (Cache.TypeIgnoreInUpdateProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var ignoreInUpdateProperties = type.GetTypeProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is IgnoreInUpdateAttribute)).ToList();
            Cache.TypeIgnoreInUpdateProperties[type.TypeHandle] = ignoreInUpdateProperties;
            return ignoreInUpdateProperties;
        }


        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties with Key Attribute
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeKeyProperties(this Type type)
        {
            if (Cache.TypeKeyProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var keyProperties = type.GetTypeProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is KeyAttribute)).ToList();
            Cache.TypeKeyProperties[type.TypeHandle] = keyProperties;
            return keyProperties;
        }


        /// <summary>
        /// Get the list of PropertyinfoExtended cached for this Type that rappresent the properties with Not Write Attribute
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static List<PropertyInfo> GetTypeNotWriteProperties(this Type type)
        {
            if (Cache.TypeNotWriteProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> propertiesCached))
                return propertiesCached.ToList();

            var unWriteProperties = type.GetTypeProperties().Where(p => p.GetCustomAttributes(true).Any(a => a is NotWriteAttribute)).ToList();
            Cache.TypeNotWriteProperties[type.TypeHandle] = unWriteProperties;
            return unWriteProperties;
        }


        /// <summary>
        /// Get the property info that rappresent the Primary Key of the Model
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static PropertyInfo GetTypeKeyOrExplicitKeyProperty(this Type type)
        {
            var keys = type.GetTypeKeyProperties();
            var explicitKeys = type.GetTypeExplicitKeyProperties();
            var keyCount = keys.Count + explicitKeys.Count;

            return keys.Count > 0 ? keys[0] : explicitKeys.Count > 0 ? explicitKeys[0] : null;
        }

    }


}
