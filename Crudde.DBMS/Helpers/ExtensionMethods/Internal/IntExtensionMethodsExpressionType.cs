﻿using System;
using System.Linq.Expressions;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "ExpressionType" object
    /// </summary>
    internal static class IntExtensionMethodsExpressionType
    {

        /// <summary>
        /// Get operand Condition enum from an Expression Type
        /// </summary>
        /// <param name="expressionType"></param>
        /// <returns></returns>
        internal static TypeOperandCondition GetOperand(this ExpressionType expressionType)
        {
            switch (expressionType)
            {
                case ExpressionType.Add:
                    throw new NotImplementedException("The ExpressionType 'Add' is not supported yet!");

                case ExpressionType.AddAssign:
                    throw new NotImplementedException("The ExpressionType 'AddAssign' is not supported yet!");

                case ExpressionType.AddAssignChecked:
                    throw new NotImplementedException("The ExpressionType 'AddAssignChecked' is not supported yet!");

                case ExpressionType.AddChecked:
                    throw new NotImplementedException("The ExpressionType 'AddChecked' is not supported yet!");

                case ExpressionType.And:
                    return TypeOperandCondition.And;

                case ExpressionType.AndAlso:
                    return TypeOperandCondition.And;

                case ExpressionType.AndAssign:
                    throw new NotImplementedException("The ExpressionType 'AndAssign' is not supported yet!");

                case ExpressionType.ArrayIndex:
                    throw new NotImplementedException("The ExpressionType 'ArrayIndex' is not supported yet!");

                case ExpressionType.ArrayLength:
                    throw new NotImplementedException("The ExpressionType 'ArrayLength' is not supported yet!");

                case ExpressionType.Assign:
                    throw new NotImplementedException("The ExpressionType 'Assign' is not supported yet!");

                case ExpressionType.Block:
                    throw new NotImplementedException("The ExpressionType 'Block' is not supported yet!");

                case ExpressionType.Call:
                    throw new NotImplementedException("The ExpressionType 'Call' is not supported yet!");

                case ExpressionType.Coalesce:
                    throw new NotImplementedException("The ExpressionType 'Coalesce' is not supported yet!");

                case ExpressionType.Conditional:
                    throw new NotImplementedException("The ExpressionType 'Conditional' is not supported yet!");

                case ExpressionType.Constant:
                    throw new NotImplementedException("The ExpressionType 'Constant' is not supported yet!");

                case ExpressionType.Convert:
                    throw new NotImplementedException("The ExpressionType 'Convert' is not supported yet!");

                case ExpressionType.ConvertChecked:
                    throw new NotImplementedException("The ExpressionType 'ConvertChecked' is not supported yet!");

                case ExpressionType.DebugInfo:
                    throw new NotImplementedException("The ExpressionType 'DebugInfo' is not supported yet!");

                case ExpressionType.Decrement:
                    throw new NotImplementedException("The ExpressionType 'Decrement' is not supported yet!");

                case ExpressionType.Default:
                    throw new NotImplementedException("The ExpressionType 'Default' is not supported yet!");

                case ExpressionType.Divide:
                    throw new NotImplementedException("The ExpressionType 'Divide' is not supported yet!");

                case ExpressionType.DivideAssign:
                    throw new NotImplementedException("The ExpressionType 'DivideAssign' is not supported yet!");

                case ExpressionType.Dynamic:
                    throw new NotImplementedException("The ExpressionType 'Dynamic' is not supported yet!");

                case ExpressionType.Equal:
                    return TypeOperandCondition.Equal;

                case ExpressionType.ExclusiveOr:
                    throw new NotImplementedException("The ExpressionType 'ExclusiveOr' is not supported yet!");

                case ExpressionType.ExclusiveOrAssign:
                    throw new NotImplementedException("The ExpressionType 'ExclusiveOrAssign' is not supported yet!");

                case ExpressionType.Extension:
                    throw new NotImplementedException("The ExpressionType 'Extension' is not supported yet!");

                case ExpressionType.Goto:
                    throw new NotImplementedException("The ExpressionType 'Goto' is not supported yet!");

                case ExpressionType.GreaterThan:
                    return TypeOperandCondition.GreaterThan;

                case ExpressionType.GreaterThanOrEqual:
                    return TypeOperandCondition.GreaterThanOrEqual;

                case ExpressionType.Increment:
                    throw new NotImplementedException("The ExpressionType 'Increment' is not supported yet!");

                case ExpressionType.Index:
                    throw new NotImplementedException("The ExpressionType 'Index' is not supported yet!");

                case ExpressionType.Invoke:
                    throw new NotImplementedException("The ExpressionType 'Invoke' is not supported yet!");

                case ExpressionType.IsFalse:
                    throw new NotImplementedException("The ExpressionType 'IsFalse' is not supported yet!");

                case ExpressionType.IsTrue:
                    throw new NotImplementedException("The ExpressionType 'IsTrue' is not supported yet!");

                case ExpressionType.Label:
                    throw new NotImplementedException("The ExpressionType 'Label' is not supported yet!");

                case ExpressionType.Lambda:
                    throw new NotImplementedException("The ExpressionType 'Lambda' is not supported yet!");

                case ExpressionType.LeftShift:
                    throw new NotImplementedException("The ExpressionType 'LeftShift' is not supported yet!");

                case ExpressionType.LeftShiftAssign:
                    throw new NotImplementedException("The ExpressionType 'LeftShiftAssign' is not supported yet!");

                case ExpressionType.LessThan:
                    return TypeOperandCondition.LessThan;

                case ExpressionType.LessThanOrEqual:
                    return TypeOperandCondition.LessThanOrEqual;

                case ExpressionType.ListInit:
                    throw new NotImplementedException("The ExpressionType 'ListInit' is not supported yet!");

                case ExpressionType.Loop:
                    throw new NotImplementedException("The ExpressionType 'Loop' is not supported yet!");

                case ExpressionType.MemberAccess:
                    throw new NotImplementedException("The ExpressionType 'MemberAccess' is not supported yet!");

                case ExpressionType.MemberInit:
                    throw new NotImplementedException("The ExpressionType 'MemberInit' is not supported yet!");

                case ExpressionType.Modulo:
                    throw new NotImplementedException("The ExpressionType 'Modulo' is not supported yet!");

                case ExpressionType.ModuloAssign:
                    throw new NotImplementedException("The ExpressionType 'ModuloAssign' is not supported yet!");

                case ExpressionType.Multiply:
                    throw new NotImplementedException("The ExpressionType 'Multiply' is not supported yet!");

                case ExpressionType.MultiplyAssign:
                    throw new NotImplementedException("The ExpressionType 'MultiplyAssign' is not supported yet!");

                case ExpressionType.MultiplyAssignChecked:
                    throw new NotImplementedException("The ExpressionType 'MultiplyAssignChecked' is not supported yet!");

                case ExpressionType.MultiplyChecked:
                    throw new NotImplementedException("The ExpressionType 'MultiplyChecked' is not supported yet!");

                case ExpressionType.Negate:
                    throw new NotImplementedException("The ExpressionType 'Negate' is not supported yet!");

                case ExpressionType.NegateChecked:
                    throw new NotImplementedException("The ExpressionType 'NegateChecked' is not supported yet!");

                case ExpressionType.New:
                    throw new NotImplementedException("The ExpressionType 'New' is not supported yet!");

                case ExpressionType.NewArrayBounds:
                    throw new NotImplementedException("The ExpressionType 'NewArrayBounds' is not supported yet!");

                case ExpressionType.NewArrayInit:
                    throw new NotImplementedException("The ExpressionType 'NewArrayInit' is not supported yet!");

                case ExpressionType.Not:
                    throw new NotImplementedException("The ExpressionType 'Not' is not supported yet!");

                case ExpressionType.NotEqual:
                    return TypeOperandCondition.NotEqual;

                case ExpressionType.OnesComplement:
                    throw new NotImplementedException("The ExpressionType 'OnesComplement' is not supported yet!");

                case ExpressionType.Or:
                    return TypeOperandCondition.Or;

                case ExpressionType.OrAssign:
                    throw new NotImplementedException("The ExpressionType 'OrAssign' is not supported yet!");

                case ExpressionType.OrElse:
                    return TypeOperandCondition.Or;

                case ExpressionType.Parameter:
                    throw new NotImplementedException("The ExpressionType 'Parameter' is not supported yet!");

                case ExpressionType.PostDecrementAssign:
                    throw new NotImplementedException("The ExpressionType 'PostDecrementAssign' is not supported yet!");

                case ExpressionType.PostIncrementAssign:
                    throw new NotImplementedException("The ExpressionType 'PostIncrementAssign' is not supported yet!");

                case ExpressionType.Power:
                    throw new NotImplementedException("The ExpressionType 'Power' is not supported yet!");

                case ExpressionType.PowerAssign:
                    throw new NotImplementedException("The ExpressionType 'PowerAssign' is not supported yet!");

                case ExpressionType.PreDecrementAssign:
                    throw new NotImplementedException("The ExpressionType 'PreDecrementAssign' is not supported yet!");

                case ExpressionType.PreIncrementAssign:
                    throw new NotImplementedException("The ExpressionType 'PreIncrementAssign' is not supported yet!");

                case ExpressionType.Quote:
                    throw new NotImplementedException("The ExpressionType 'Quote' is not supported yet!");

                case ExpressionType.RightShift:
                    throw new NotImplementedException("The ExpressionType 'RightShift' is not supported yet!");

                case ExpressionType.RightShiftAssign:
                    throw new NotImplementedException("The ExpressionType 'RightShiftAssign' is not supported yet!");

                case ExpressionType.RuntimeVariables:
                    throw new NotImplementedException("The ExpressionType 'RuntimeVariables' is not supported yet!");

                case ExpressionType.Subtract:
                    throw new NotImplementedException("The ExpressionType 'Subtract' is not supported yet!");

                case ExpressionType.SubtractAssign:
                    throw new NotImplementedException("The ExpressionType 'SubtractAssign' is not supported yet!");

                case ExpressionType.SubtractAssignChecked:
                    throw new NotImplementedException("The ExpressionType 'SubtractAssignChecked' is not supported yet!");

                case ExpressionType.SubtractChecked:
                    throw new NotImplementedException("The ExpressionType 'SubtractChecked' is not supported yet!");

                case ExpressionType.Switch:
                    throw new NotImplementedException("The ExpressionType 'Switch' is not supported yet!");

                case ExpressionType.Throw:
                    throw new NotImplementedException("The ExpressionType 'Throw' is not supported yet!");

                case ExpressionType.Try:
                    throw new NotImplementedException("The ExpressionType 'Try' is not supported yet!");

                case ExpressionType.TypeAs:
                    throw new NotImplementedException("The ExpressionType 'TypeAs' is not supported yet!");

                case ExpressionType.TypeEqual:
                    throw new NotImplementedException("The ExpressionType 'TypeEqual' is not supported yet!");

                case ExpressionType.TypeIs:
                    throw new NotImplementedException("The ExpressionType 'TypeIs' is not supported yet!");

                case ExpressionType.UnaryPlus:
                    throw new NotImplementedException("The ExpressionType 'UnaryPlus' is not supported yet!");

                case ExpressionType.Unbox:
                    throw new NotImplementedException("The ExpressionType 'Unbox' is not supported yet!");

                default:
                    throw new NotImplementedException("The ExpressionType '" + expressionType.ToString() + @"' is not supported yet!");
            }
        }

    }
}
