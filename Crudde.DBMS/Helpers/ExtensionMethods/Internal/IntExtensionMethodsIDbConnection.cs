﻿using System.Data;
using PaironsTech.Crudde.DBMS.SqlAdapters;

namespace PaironsTech.Crudde.DBMS.Helpers.ExtensionMethods
{

    /// <summary>
    /// Internal static class that contain extension methods for "IDbConnection" object
    /// </summary>
    internal static class IntExtensionMethodsIDbConnection
    {

        /// <summary>
        /// Get the type of DB base on the connection
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <returns></returns>
        internal static ISqlAdapter GetAdapter(this IDbConnection connection)
        {
            var name = connection.GetType().Name.ToLower();

            return !Cache.AdapterDictionary.ContainsKey(name)
                ? Cache.DefaultAdapter
                : Cache.AdapterDictionary[name];
        }

    }
}
