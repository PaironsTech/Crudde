﻿using PaironsTech.Crudde.DBMS.SqlAdapters;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace PaironsTech.Crudde.DBMS.Helpers
{

    /// <summary>
    /// Static Class with static var for Chacing.
    /// </summary>
    internal static partial class Cache
    {

        /// <summary>
        /// Default Adapter
        /// </summary>
        internal static readonly ISqlAdapter DefaultAdapter = new SqlAdapterPostgres();

        /// <summary>
        /// Dictionary of Adapters [string name connection - SqlAdapter]
        /// </summary>
        internal static readonly Dictionary<string, ISqlAdapter> AdapterDictionary = new Dictionary<string, ISqlAdapter>
        {
            ["sqlconnection"] = new SqlAdapterSqlServer(),
            ["sqlceconnection"] = new SqlAdapterSqlCeServer(),
            ["npgsqlconnection"] = new SqlAdapterPostgres(),
            ["sqliteconnection"] = new SqlAdapterSQLite(),
            ["mysqlconnection"] = new SqlAdapterMySql(),
            ["fbconnection"] = new SqlAdapterFirebird()
        };


        /// <summary>
        /// Cache of Type and Table / View Name associated
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, string> TypeTableAndViewName = new ConcurrentDictionary<RuntimeTypeHandle, string>();

        /// <summary>
        /// Cache of properties of the Type 
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();


        // Cachig Attributes of Model Props

        /// <summary>
        /// Cache of ExplicitKey properties of the Type
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeExplicitKeyProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// Cache of ForeignKeyMapped properties of the Type
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeForeignKeyMappedProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// Cache of IgnoreInInsert properties of the Type
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeIgnoreInInsertProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// Cache of IgnoreInUpdate properties of the Type
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeIgnoreInUpdateProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// Cache of Key properties of the Type
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeKeyProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

        /// <summary>
        /// Cache of NotWrite properties of the Type
        /// </summary>
        internal static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeNotWriteProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();

    }
}
