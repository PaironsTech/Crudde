﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlParameters
{

    /// <summary>
    /// Object that help us to insert in query external paramValue
    /// </summary>
    public class ParamValue
    {

        /// <summary>
        /// The name of the key of the value in param object
        /// </summary>
        public string NameParamValue { get; }   

        /// <summary>
        /// Return an object value
        /// </summary>
        public object GetValue { get { return null; } }

        /// <summary>
        /// Return a int value
        /// </summary>
        public int GetValueInt { get { return 0; } }

        /// <summary>
        /// Return a long value
        /// </summary>
        public long GetValueLong { get { return 0; } }

        /// <summary>
        /// Return a string value
        /// </summary>
        public string GetValueString { get { return string.Empty; } }



        /// <summary>
        /// Constructor of 'ParamValue' object
        /// </summary>
        public ParamValue(string nameKeyParamValue)
        {
            NameParamValue = nameKeyParamValue;
        }



        

    }

}
