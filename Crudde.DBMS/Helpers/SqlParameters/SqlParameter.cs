﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlParameters
{

    /// <summary>
    /// Internal static class for manage Sql Parameters dynamically
    /// </summary>
    internal static class SqlParameter
    {

        /// <summary>
        /// Static readonly random object for generate random string
        /// </summary>
        private static readonly Random Random = new Random();

        /// <summary>
        /// Return the name of the key of the dictionary that corresponding at the value passed and insert
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="value"></param>
        /// <param name="dictionaryUpdated"></param>
        /// <param name="possibleName"></param>
        /// <returns></returns>
        internal static string GetNameParam(Dictionary<string, object> dictionary, object value, out Dictionary<string, object> dictionaryUpdated, string possibleName = null)
        {
            if (string.IsNullOrEmpty(possibleName))
                possibleName = GenerateRandomNameKey();

            bool continueLoop = true;

            do
            {
                if (!dictionary.ContainsKey(possibleName))
                {
                    dictionary[possibleName] = value;
                    continueLoop = false;
                }
                else
                    possibleName = GenerateRandomNameKey();

            } while (continueLoop);

            dictionaryUpdated = dictionary;
            return possibleName;
        }

        /// <summary>
        /// Generate random string of letter of length between 5 and 9
        /// </summary>
        /// <returns></returns>
        private static string GenerateRandomNameKey()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, Random.Next(5, 10))
              .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

    }

}
