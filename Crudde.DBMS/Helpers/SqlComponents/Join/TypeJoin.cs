﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{

    /// <summary>
    /// Rappresent the different Type of Join
    /// </summary>
    internal enum TypeJoin
    {

        /// <summary>
        /// Rappresent the "INNER JOIN" clause of SQL Query
        /// </summary>
        InnerJoin,

        /// <summary>
        /// Rappresent the "LEFT JOIN" clause of SQL Query
        /// </summary>
        LeftJoin,

        /// <summary>
        /// Rappresent the "RIGHT JOIN" clause of SQL Query
        /// </summary>
        RightJoin

    }

}
