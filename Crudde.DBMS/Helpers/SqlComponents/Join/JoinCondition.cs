﻿using System;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{

    /// <summary>
    /// This object rappresent the Join Statement with the table to join, the type of join and the condition
    /// </summary>
    internal class JoinCondition
    {

        /// <summary>
        /// The type of the Model to join
        /// </summary>
        internal Type TypeModelToJoin { get; set; }

        /// <summary>
        /// The condition of the Join
        /// </summary>
        internal Condition ConditionToJoin { get; set; }

        /// <summary>
        /// The type of the join
        /// </summary>
        internal TypeJoin TypeJoin { get; set; }
    }

}
