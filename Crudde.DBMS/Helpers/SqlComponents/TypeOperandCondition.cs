﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{

    /// <summary>
    /// Rappresent the Operand of a condition in a SQL Query
    /// </summary>
    internal enum TypeOperandCondition
    {

        /// <summary>
        /// Rappresent the "=" Operand in a condition in a SQL Query
        /// </summary>
        Equal,

        /// <summary>
        /// Rappresent the "!=" Operand in a condition in a SQL Query
        /// </summary>
        NotEqual,

        /// <summary>
        /// Rappresent the "AND" Operand in a condition in a SQL Query
        /// </summary>
        And,

        /// <summary>
        /// Rappresent the "OR" Operand in a condition in a SQL Query
        /// </summary>
        Or,

        /// <summary>
        /// Rappresent the ">" Operand in a condition in a SQL Query
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Rappresent the ">=" Operand in a condition in a SQL Query
        /// </summary>
        GreaterThanOrEqual,

        /// <summary>
        /// Rappresent the "less than" Operand in a condition in a SQL Query
        /// </summary>
        LessThan,

        /// <summary>
        /// Rappresent the "less than or =" Operand in a condition in a SQL Query
        /// </summary>
        LessThanOrEqual,

        /// <summary>
        /// Rappresent the "LIKE" Operand in a condition in a SQL Query
        /// </summary>
        Like,

        /// <summary>
        /// Rappresent the "ILIKE" Operand in a condition in a SQL Query
        /// </summary>
        ILike,

        /// <summary>
        /// Rappresent the "IN" Operand in a condition in a SQL Query
        /// </summary>
        In

    }

}
