﻿using System.Reflection;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{

    /// <summary>
    /// Rappresent the Object for record the Order By in Variable
    /// </summary>
    internal class OrderByInfo
    {

        /// <summary>
        /// Contains PropertyInfoExteded
        /// </summary>
        internal PropertyInfo Property { get; set; }

        /// <summary>
        /// Contains the Type of the Order By
        /// </summary>
        internal TypeOrderBy TypeOrderBy { get; set; }

    }

}

