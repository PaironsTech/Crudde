﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{
    
    /// <summary>
    /// Rappresent the different Type of Order By
    /// </summary>
    internal enum TypeOrderBy
    {

        /// <summary>
        /// Rappresent "ORDER BY COLUMN ASC" clause in a SQL Query
        /// </summary>
        OrderByAscending,

        /// <summary>
        /// Rappresent "ORDER BY COLUMN DESC" clause in a SQL Query
        /// </summary>
        OrderByDescending

    }

}
