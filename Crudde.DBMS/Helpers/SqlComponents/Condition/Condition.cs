﻿using System.Reflection;

namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{

    /// <summary>
    /// Rappreset the Condition of a SQL Query
    /// </summary>
    internal class Condition
    {

        /// <summary>
        /// This boolean is usefull for understand if we need to get value from property that reference 'Node Condition' or 'Condition'
        /// </summary>
        internal bool IsNode { get; set; }


        // Condition Properties


        /// <summary>
        /// The left node of a condition
        /// </summary>
        internal Condition ConditionLeftNode { get; set; }

        /// <summary>
        /// The operando of a condition
        /// </summary>
        internal TypeOperandCondition? ConditionOperand { get; set; }

        /// <summary>
        /// The right node of a condition
        /// </summary>
        internal Condition ConditionRightNode { get; set; }


        // Node Condition Properties


        /// <summary>
        /// The type of Node Condition
        /// </summary>
        internal TypeNodeCondition? NodeConditionType { get; set; }

        /// <summary>
        /// Node condition property
        /// </summary>
        internal PropertyInfo NodeConditionProperty { get; set; }

        /// <summary>
        /// Node condition value
        /// </summary>
        internal object NodeConditionValue { get; set; }

        /// <summary>
        /// Node Condition Name of Property that contain the value in param object
        /// </summary>
        internal string NameParamValue { get; set; }




        /// <summary>
        /// Constructor of Condition
        /// </summary>
        public Condition()
        {
            IsNode = false;
            ConditionLeftNode = null;
            ConditionOperand = null;
            ConditionRightNode = null;
            NodeConditionType = null;
            NodeConditionProperty = null;
            NodeConditionValue = null;
        }

    }
}
