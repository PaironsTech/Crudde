﻿namespace PaironsTech.Crudde.DBMS.Helpers.SqlComponents
{

    /// <summary>
    /// Rappresent the Type of NodeCondition
    /// </summary>
    internal enum TypeNodeCondition
    {

        /// <summary>
        /// The Node Condition is a Property
        /// </summary>
        Property,

        /// <summary>
        /// The Node condition is a Constant
        /// </summary>
        Constant,

        /// <summary>
        /// The Node condition is a Param Value
        /// </summary>
        ParamValue

    }

}
