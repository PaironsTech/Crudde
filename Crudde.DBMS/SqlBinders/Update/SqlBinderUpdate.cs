﻿using System.Collections.Generic;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;

namespace PaironsTech.Crudde.DBMS.SqlBinders
{

    /// <summary>
    /// SqlBinderUpdate it's used to contain information about Update Sql Command
    /// </summary>
    /// <typeparam name="TModelToUpdate"></typeparam>
    public class SqlBinderUpdate<TModelToUpdate> where TModelToUpdate : IModelUpdatable
    {

        /// <summary>
        /// Dictionary with property info that reference the column and its value.
        /// </summary>
        internal Dictionary<PropertyInfo, object> ColumnsValues = new Dictionary<PropertyInfo, object>();

        /// <summary>
        /// The Where clause of Update Sql Command
        /// </summary>
        internal Condition WhereClause = null;

    }
}
