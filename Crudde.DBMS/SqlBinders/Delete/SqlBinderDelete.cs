﻿using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;

namespace PaironsTech.Crudde.DBMS.SqlBinders
{

    /// <summary>
    /// SqlBinderDelete it's used to contain information about Delete Sql Command
    /// </summary>
    /// <typeparam name="TModelToDelete"></typeparam>
    public class SqlBinderDelete<TModelToDelete> where TModelToDelete : IModelDeletable
    {

        /// <summary>
        /// Condition of Delete Sql Command
        /// </summary>
        internal Condition WhereCondition = null;

    }

}
