﻿using System.Collections.Generic;
using System.Reflection;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;

namespace PaironsTech.Crudde.DBMS.SqlBinders
{

    /// <summary>
    /// SqlBinderInsert it's used to contain information about Insert Sql Command
    /// </summary>
    public class SqlBinderInsert<TModelToInsert> where TModelToInsert : IModelInsertable
    {

        /// <summary>
        /// Primary Key of the table to insert data
        /// </summary>
        internal PropertyInfo PrimaryKey = null;

        /// <summary>
        /// Dictionary with property info that reference the column and its value.
        /// </summary>
        internal Dictionary<PropertyInfo, object> ColumnsValues = new Dictionary<PropertyInfo, object>();

    }

}
