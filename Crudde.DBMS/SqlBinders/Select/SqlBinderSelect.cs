﻿using System.Collections.Generic;
using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;
using PaironsTech.Crudde.DBMS.Helpers.SqlComponents;

namespace PaironsTech.Crudde.DBMS.SqlBinders
{

    /// <summary>
    /// SqlBinderSelect it's used to contain information about Select Sql Command
    /// </summary>
    public class SqlBinderSelect<TModelFrom> where TModelFrom : IModelSelectable
    {

        /// <summary>
        /// If is checked the query contains "Distinct" value.
        /// </summary>
        internal bool Distinct = false;

        /// <summary>
        /// List of Join Condition
        /// </summary>
        internal List<JoinCondition> ListJoins = new List<JoinCondition>();

        /// <summary>
        /// Condition of the WHERE state of the query.
        /// </summary>
        internal Condition WhereCondition = null;

        /// <summary>
        /// List with the information for set the ORDER BY state of the query
        /// </summary>
        internal List<OrderByInfo> ListOrderBy = new List<OrderByInfo>();

        /// <summary>
        /// If is not null, set the LIMIT of result of the query
        /// </summary>
        internal int? Limit = null;

        /// <summary>
        /// If is not null, set the OFFSET of result of the query
        /// </summary>
        internal int? Offset = null;
            
    }

}
