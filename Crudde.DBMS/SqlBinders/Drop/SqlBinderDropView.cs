﻿namespace PaironsTech.Crudde.DBMS.SqlBinders
{

    /// <summary>
    /// SqlBinderDropViews it's used to contain information about Drop View Sql Command
    /// </summary>
    public class SqlBinderDropView<TModelToDrop> where TModelToDrop : ICruddeView
    {

        /// <summary>
        /// Check if control if View exist
        /// </summary>
        internal bool CheckIfExist = false;

        /// <summary>
        /// Check if Drop on Cascade of use default Restrict
        /// </summary>
        internal bool DropCascade = false;

    }

}
