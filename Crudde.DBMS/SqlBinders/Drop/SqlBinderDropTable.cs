﻿namespace PaironsTech.Crudde.DBMS.SqlBinders
{

    /// <summary>
    /// SqlBinderDropTables it's used to contain information about Drop Table Sql Command
    /// </summary>
    public class SqlBinderDropTable<TModelToDrop> where TModelToDrop : ICruddeTable
    {

        /// <summary>
        /// Check if control if Table exist
        /// </summary>
        internal bool CheckIfExist = false;

        /// <summary>
        /// Check if Drop on Cascade of use default Restrict
        /// </summary>
        internal bool DropCascade = false;

    }

}
