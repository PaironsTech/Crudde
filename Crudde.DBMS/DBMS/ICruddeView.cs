﻿using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;

namespace PaironsTech.Crudde.DBMS
{

    /// <summary>
    /// Interface of Views
    /// </summary>
    public interface ICruddeView : IModelSelectable, IModelDroppable { }

}
