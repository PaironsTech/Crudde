﻿using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;

namespace PaironsTech.Crudde.DBMS
{

    /// <summary>
    /// Interface of Tables
    /// </summary>
    public interface ICruddeTable : IModelDeletable, IModelDroppable, IModelInsertable, IModelSelectable, IModelUpdatable { }

}
