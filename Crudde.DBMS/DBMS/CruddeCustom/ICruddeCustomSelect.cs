﻿using PaironsTech.Crudde.DBMS.Helpers.SqlBuilderAcceptance;

namespace PaironsTech.Crudde.DBMS
{

    /// <summary>
    /// Interface of Custom Select
    /// </summary>
    public interface ICruddeCustomSelect : IModelSelectable { }

}
