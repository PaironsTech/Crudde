﻿using System;
using System.Reflection;

#if NETSTANDARD1_3 || NETCOREAPP1_0
using System.Collections.Generic;
#endif

namespace PaironsTech.Crudde
{

    /// <summary>
    /// Static class with extension method for Type object.
    /// </summary>
    public static class TypeExtensions
    {

        /// <summary>
        /// Get Name of Type
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>String with the type name</returns>
        public static string Name(this Type type) =>
#if NETSTANDARD1_3 || NETCOREAPP1_0
                type.GetTypeInfo().Name;
#else
                type.Name;
#endif

        /// <summary>
        /// Check if type is value
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>Bool with value if is value type</returns>
        public static bool IsValueType(this Type type) =>
#if NETSTANDARD1_3 || NETCOREAPP1_0
                type.GetTypeInfo().IsValueType;
#else
                type.IsValueType;
#endif

        /// <summary>
        /// Check if type is Enum
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>Bool that check if type is Enum</returns>
        public static bool IsEnum(this Type type) =>
#if NETSTANDARD1_3 || NETCOREAPP1_0
                type.GetTypeInfo().IsEnum;
#else
                type.IsEnum;
#endif

        /// <summary>
        /// Check if type is generic type
        /// </summary>
        /// <param name="type">type</param>
        /// <returns>bool that check if type is generic type</returns>
        public static bool IsGenericType(this Type type) =>
#if NETSTANDARD1_3 || NETCOREAPP1_0
                type.GetTypeInfo().IsGenericType;
#else
                type.IsGenericType;
#endif

        /// <summary>
        /// Check if type is interface
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>Bool that check if type is interface</returns>
        public static bool IsInterface(this Type type) =>
#if NETSTANDARD1_3 || NETCOREAPP1_0
                type.GetTypeInfo().IsInterface;
#else
                type.IsInterface;
#endif

#if NETSTANDARD1_3 || NETCOREAPP1_0
        
        /// <summary>
        /// Get Curom Attributes
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="inherit">Type</param>
        /// <returns>IEnumerable Attribute</returns>
        public static IEnumerable<Attribute> GetCustomAttributes(this Type type, bool inherit)
        {
            return type.GetTypeInfo().GetCustomAttributes(inherit);
        }

        /// <summary>
        /// Get Type Code
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>Tyep code</returns>
        public static TypeCode GetTypeCode(Type type)
        {
            if (type == null) return TypeCode.Empty;
            if (typeCodeLookup.TryGetValue(type, out TypeCode result)) return result;

            if (type.IsEnum())
            {
                type = Enum.GetUnderlyingType(type);
                if (typeCodeLookup.TryGetValue(type, out result)) return result;
            }
            return TypeCode.Object;
        }

        private static readonly Dictionary<Type, TypeCode> typeCodeLookup = new Dictionary<Type, TypeCode>
        {
            [typeof(bool)] = TypeCode.Boolean,
            [typeof(byte)] = TypeCode.Byte,
            [typeof(char)] = TypeCode.Char,
            [typeof(DateTime)] = TypeCode.DateTime,
            [typeof(decimal)] = TypeCode.Decimal,
            [typeof(double)] = TypeCode.Double,
            [typeof(short)] = TypeCode.Int16,
            [typeof(int)] = TypeCode.Int32,
            [typeof(long)] = TypeCode.Int64,
            [typeof(object)] = TypeCode.Object,
            [typeof(sbyte)] = TypeCode.SByte,
            [typeof(float)] = TypeCode.Single,
            [typeof(string)] = TypeCode.String,
            [typeof(ushort)] = TypeCode.UInt16,
            [typeof(uint)] = TypeCode.UInt32,
            [typeof(ulong)] = TypeCode.UInt64,
        };

#else
        
        /// <summary>
        /// Get Type Code
        /// </summary>
        /// <param name="type">Type</param>
        /// <returns>Type Code</returns>
        public static TypeCode GetTypeCode(Type type) => Type.GetTypeCode(type);

#endif

        /// <summary>
        /// Get public instance Methpd
        /// </summary>
        /// <param name="type">type</param>
        /// <param name="name">name</param>
        /// <param name="types">Array of type</param>
        /// <returns>MethodInfo</returns>
        public static MethodInfo GetPublicInstanceMethod(this Type type, string name, Type[] types)
        {
#if NETSTANDARD1_3 || NETCOREAPP1_0
                var method = type.GetMethod(name, types);
                return (method?.IsPublic == true && !method.IsStatic) ? method : null;
#else
                return type.GetMethod(name, BindingFlags.Instance | BindingFlags.Public, null, types, null);
#endif
        }
    }
}
